package com.example.android.continguesrecord.Utils;

import android.content.ContentValues;
import android.content.Context;
import android.provider.MediaStore;

import com.example.android.continguesrecord.MyVideo;

import java.io.File;

public class PathUtils {

    public static final String TAG = PathUtils.class.getSimpleName();

    public static String getVideoFilePath(Context context,String dateInMillis) {
        final File dir = context.getExternalFilesDir(null);
        return (dir == null ? "" : (dir.getAbsolutePath() + "/"))
                + dateInMillis + ".mp4";
    }

    public static String createImagePath(Context context, String dateInMillis){
        final File dir = context.getExternalFilesDir(null);
        return (dir == null ? "" :
                (dir.getAbsolutePath() + "/IMG_"))
                + dateInMillis + ".jpeg";
    }

    /*
    Will be called when the user want to save a video to the public videos library.
     */
    public static ContentValues createFinalPath(Context context,MyVideo video)
    {
        File dir = context.getExternalFilesDir(null);
        String title = video.getTitle() + CalendarUtils.getDateFromMillis(video.getDateInMills());
        String filename = title + ".mp4";
        String filePath = (dir == null ? "" : (dir.getAbsolutePath() + "/")) + filename;
        ContentValues values = new ContentValues(7);
        values.put(MediaStore.Video.Media.TITLE, title);
        values.put(MediaStore.Video.Media.DISPLAY_NAME, filename);
        values.put(MediaStore.Video.Media.DATE_TAKEN, CalendarUtils.getDateFromMillis(video.getDateInMills()));
        values.put(MediaStore.Video.Media.MIME_TYPE, "video/mp4");
        values.put(MediaStore.Video.Media.DATA, filePath);

        return values;
    }
}
