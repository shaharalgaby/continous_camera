package com.example.android.continguesrecord.services;

import android.content.ContentValues;
import android.content.Intent;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.JobIntentService;
import android.util.Log;
import android.widget.Toast;

import com.coremedia.iso.boxes.Container;
import com.example.android.continguesrecord.DB.VideosViewModel;
import com.example.android.continguesrecord.MyVideo;
import com.example.android.continguesrecord.Utils.CONSTANTS;
import com.example.android.continguesrecord.Utils.PathUtils;
import com.example.android.continguesrecord.Utils.VideoUtils;
import com.example.android.continguesrecord.Utils.ffmpegCmdUtils;
import com.github.hiteshsondhi88.libffmpeg.ExecuteBinaryResponseHandler;
import com.googlecode.mp4parser.authoring.Movie;
import com.googlecode.mp4parser.authoring.Track;
import com.googlecode.mp4parser.authoring.builder.DefaultMp4Builder;
import com.googlecode.mp4parser.authoring.container.mp4.MovieCreator;
import com.googlecode.mp4parser.authoring.tracks.AppendTrack;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;


public class MergeVideosService extends JobIntentService {

    private static final String TAG = MergeVideosService.class.getSimpleName();
    private MyVideo myVideo;
    private String finalPath;
    private String[] videos;
    private ArrayList<String> filesToDelete;
    final VideosViewModel mViewModel = new
            VideosViewModel(getApplication());
    private ContentValues contentValues;

    public MergeVideosService() {
        super();
    }

    @Override
    protected void onHandleWork(@NonNull Intent intent) {
        Log.i(TAG,"Starting the merge service.");

        myVideo = intent.getParcelableExtra(CONSTANTS.INTENT_EXTRA_VIDEO);

        ArrayList<String> videos = myVideo.getVideos();
        this.videos = new String[videos.size()];
        for(int i = 0 ; i < videos.size(); i++){
            this.videos[i] = videos.get(i);
        }

        contentValues = PathUtils.createFinalPath(this,myVideo);
        finalPath = (String)contentValues.get(MediaStore.Video.Media.DATA);

        filesToDelete = new ArrayList<>();

        //startMergeProcess();
        try {
            appendVideo();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String appendVideo() throws IOException {
        Log.v(TAG, "in appendVideo() videos length is " + videos.length);
        Movie[] inMovies = new Movie[videos.length];
        int index = 0;
        for(String video: videos)
        {
            Log.i(TAG, "    in appendVideo one video path = " + video);
            inMovies[index] = MovieCreator.build(video);
            index++;
        }
        List<Track> videoTracks = new LinkedList<Track>();
        List<Track> audioTracks = new LinkedList<Track>();
        for (Movie m : inMovies) {
            for (Track t : m.getTracks()) {
                if (t.getHandler().equals("soun")) {
                    audioTracks.add(t);
                }
                if (t.getHandler().equals("vide")) {
                    videoTracks.add(t);
                }
            }
        }

        Movie result = new Movie();
        Log.v(TAG, "audioTracks size = " + audioTracks.size()
                + " videoTracks size = " + videoTracks.size());
        if (audioTracks.size() > 0) {
            result.addTrack(new AppendTrack(audioTracks.toArray(new Track[audioTracks.size()])));
        }
        if (videoTracks.size() > 0) {
            result.addTrack(new AppendTrack(videoTracks.toArray(new Track[videoTracks.size()])));
        }
        Container out = new DefaultMp4Builder().build(result);
        FileChannel fc = new RandomAccessFile(finalPath, "rw").getChannel();
        out.writeContainer(fc);
        fc.close();
        Log.v(TAG, "after combine videoCombinepath = " + finalPath);

        getContentResolver().insert(MediaStore.Video.Media.EXTERNAL_CONTENT_URI,contentValues);

        return finalPath;
    }


    public void startMergeProcess(){
        String[] cmd = ffmpegCmdUtils.buildMergeVideosCmd2(
                videos,
                finalPath);

        //Start executing the command
        VideoUtils.execFFmpegBinary(this, cmd, mergeHandler);
    }

    ExecuteBinaryResponseHandler mergeHandler = new ExecuteBinaryResponseHandler(){

        @Override
        public void onSuccess(String s) {
            Log.i(TAG, "Merge command completed successfully " + s);
            Toast.makeText(MergeVideosService.this, "File saved successfully.", Toast.LENGTH_SHORT).show();
            getContentResolver().insert(MediaStore.Video.Media.EXTERNAL_CONTENT_URI,contentValues);
            cleanFiles();
        }

        @Override
        public void onProgress(String message) {
            super.onProgress(message);
            Log.i(TAG,message);
        }

        @Override
        public void onFailure(String s) {
            Log.i(TAG, "Command merge failed: " + s);
            Toast.makeText(getBaseContext(),
                    "Sorry, We couldn't save the video.", Toast.LENGTH_SHORT).show();

            filesToDelete.add(finalPath);

            cleanFiles();
        }
    };

    public void cleanFiles(){
        Log.i(TAG,"Cleaning the files.");
        for(String s : filesToDelete){
            mDeleteFile(s);
        }

        myVideo.setProcessing(false);
        mViewModel.update(myVideo);
    }

    public static void mDeleteFile(String url){
        File file = new File(url);
        if(file.delete()){
            Log.i(TAG,"The file "+url+" has successfully deleted.");
        } else {
            Log.i(TAG,"Failed to delete "+url);
        }
    }
}