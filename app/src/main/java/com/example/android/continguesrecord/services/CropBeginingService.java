package com.example.android.continguesrecord.services;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.JobIntentService;
import android.util.Log;
import android.widget.Toast;

import com.example.android.continguesrecord.DB.VideosViewModel;
import com.example.android.continguesrecord.MyVideo;
import com.example.android.continguesrecord.Utils.CONSTANTS;
import com.example.android.continguesrecord.Utils.PathUtils;
import com.example.android.continguesrecord.Utils.VideoUtils;
import com.example.android.continguesrecord.Utils.ffmpegCmdUtils;
import com.github.hiteshsondhi88.libffmpeg.ExecuteBinaryResponseHandler;

import java.io.File;
import java.util.ArrayList;

public class CropBeginingService extends JobIntentService {

    private final static String TAG = CropBeginingService.class.getSimpleName();

    private String finalPath;
    private String videoPath;
    private MyVideo mVideo;
    private ArrayList<String> filedToDelete = new ArrayList<>();

    final VideosViewModel mViewModel = new
            VideosViewModel(getApplication());

    @Override
    protected void onHandleWork(@NonNull Intent intent) {
        videoPath = intent.getStringExtra(CONSTANTS.INTENT_EXTRA_VIDEO_PATH);
        finalPath = PathUtils.getVideoFilePath(this,System.currentTimeMillis()+"");
        mVideo = intent.getParcelableExtra(CONSTANTS.INTENT_EXTRA_VIDEO);

        startTrimProcess();
    }

    public void startTrimProcess(){
        String[] cmdTrim;

        cmdTrim = ffmpegCmdUtils.cutBeginingCmd(videoPath, finalPath);

        //Start executing the command
        VideoUtils.execFFmpegBinary(this,cmdTrim, trimHandler);
    }

    ExecuteBinaryResponseHandler trimHandler = new ExecuteBinaryResponseHandler(){
        @Override
        public void onSuccess(String message) {
            super.onSuccess(message);
            Log.i(TAG,"Video trimmed Successfully!! " + message);

            //change the location of the video
            mVideo.getVideos().set(mVideo.getVideos().indexOf(videoPath),finalPath);
            mVideo.setDuration(mVideo.getDuration() + (int)VideoUtils.getVideoDuration(finalPath));
            filedToDelete.add(videoPath);

            Toast.makeText(CropBeginingService.this,
                    "Video added successfully.", Toast.LENGTH_SHORT).show();

            finishProcess();
        }

        @Override
        public void onFailure(String message) {
            super.onFailure(message);
            Log.i(TAG,"Failed to trim video!!" + message);

            mVideo.setDuration(mVideo.getDuration() + (int)VideoUtils.getVideoDuration(videoPath));
            filedToDelete.add(finalPath);

            finishProcess();
        }
    };

    private void finishProcess(){
        for(String s : filedToDelete){
            mDeleteFile(s);
        }

        mVideo.setProcessing(false);
        mViewModel.update(mVideo);
    }

    public static void mDeleteFile(String url){
        File file = new File(url);
        if(file.delete()){
            Log.i(TAG,"The file "+url+" has successfully deleted.");
        } else {
            Log.i(TAG,"Failed to delete "+url);
        }
    }
}
