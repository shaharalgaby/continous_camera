package com.example.android.continguesrecord.DB;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;
import android.arch.persistence.room.migration.Migration;
import android.content.Context;
import android.support.annotation.NonNull;

import com.example.android.continguesrecord.Utils.CONSTANTS;
import com.example.android.continguesrecord.MyVideo;

@Database(entities = {MyVideo.class}, version = 5)
@TypeConverters({Converters.class})
public abstract class VideosRoomDatabase extends RoomDatabase {

    public abstract VideoDao videoDao();

    private static volatile VideosRoomDatabase INSTANCE;

    static VideosRoomDatabase getDatabase(final Context context){
        if(INSTANCE == null){
            synchronized (VideosRoomDatabase.class) {
                if (INSTANCE == null){
                    //create the database
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            VideosRoomDatabase.class, CONSTANTS.DATABASE_NAME)
                            .build();
                }
            }
        }
        return INSTANCE;
    }

    private static final Migration MITGRATION_1_2 = new Migration(1,2){
        /**
         * Should run the necessary migrations.
         * <p>
         * This class cannot access any generated Dao in this method.
         * <p>
         * This method is already called inside a transaction and that transaction might actually be a
         * composite transaction of all necessary {@code Migration}s.
         *
         * @param database The database instance
         */
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            database.execSQL("ALTER TABLE my_videos "
                + " ADD COLUMN isProcessing INTEGER NOT NULL DEFAULT 0");
        }
    };
}
