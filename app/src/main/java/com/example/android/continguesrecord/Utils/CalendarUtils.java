package com.example.android.continguesrecord.Utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class CalendarUtils {

    public static String getTimeFromMillis(String millis){

        SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");

        Calendar cl = Calendar.getInstance();
        cl.setTimeInMillis(Long.parseLong(millis));

        return timeFormat.format(cl.getTime());
    }

    public static String getDateFromMillis(String millis){

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yy");

        Calendar cl = Calendar.getInstance();
        cl.setTimeInMillis(Long.parseLong(millis));  //here your time in miliseconds

        return dateFormat.format(cl.getTime());
    }

    public static String getProperDateRepresentation(String millis){

        Calendar videoCalendar = Calendar.getInstance();
        videoCalendar.setTimeInMillis(Long.parseLong(millis));

        Calendar nowCalendar = Calendar.getInstance();
        nowCalendar.setTimeInMillis(System.currentTimeMillis());

        //TODO need to be implemented.

        return null;
    }

}
