package com.example.android.continguesrecord.DB;
import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.example.android.continguesrecord.MyVideo;

import java.util.List;

@Dao
public interface VideoDao {

    @Insert
    void insert(MyVideo myVideo);

    @Delete
    void delete(MyVideo myVideo);

    @Update
    void update(MyVideo myVideo);

    @Query("SELECT * from my_videos ORDER BY dateInMills DESC")
    LiveData<List<MyVideo>> getAllVideos();

    @Query("SELECT * from my_videos WHERE dateInMills=:id")
    LiveData<MyVideo> getVideo(long id);

}
