package com.example.android.continguesrecord.listeners;

public interface HorizontalImageInteractions {
    void onImageClicked(int position);

    //can add more interactions such as long press and so on
}
