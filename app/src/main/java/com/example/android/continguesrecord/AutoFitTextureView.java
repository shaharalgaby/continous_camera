package com.example.android.continguesrecord;

/*
 * Copyright 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.TextureView;
import com.example.android.continguesrecord.activities.CamActivity;

/**
 * A {@link TextureView} that can be adjusted to a specified aspect ratio.
 */
public class AutoFitTextureView extends TextureView {

    private static final String TAG = AutoFitTextureView.class.getSimpleName();

    private final static int ZOOM_SENSITIVITY = 10;

    //Zooming
    private Rect mOriginalRect;
    private Rect mCurrentZoomRect;
    private float fingerSpacing = 0;
    private float zoomLevel = 1f;
    private float maximumZoomLevel;

    private Context mContext;

    public AutoFitTextureView(Context context) {
        this(context, null);
        this.mContext = context;
    }

    public AutoFitTextureView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public AutoFitTextureView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void setMaxZoomLevel(float level){
        this.maximumZoomLevel = level;
    }

    public void setZoomRect(Rect rect){
        this.mCurrentZoomRect = rect;
        this.mOriginalRect = rect;
    }

    public void setContext(Context context){
        this.mContext = context;
    }

    /**
     * Sets the aspect ratio for this view. The size of the view will be measured based on the ratio
     * calculated from the parameters. Note that the actual sizes of parameters don't matter, that
     * is, calling setAspectRatio(2, 3) and setAspectRatio(4, 6) make the same result.
     *
     * @param width  Relative horizontal size
     * @param height Relative vertical size
     */
    public void setAspectRatio(int width, int height) {
        if (width < 0 || height < 0) {
            throw new IllegalArgumentException("Size cannot be negative.");
        }
        requestLayout();
    }

//    @Override
//    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
//        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
//        int width = MeasureSpec.getSize(widthMeasureSpec);
//        int height = MeasureSpec.getSize(heightMeasureSpec);
//        if (0 == mRatioWidth || 0 == mRatioHeight) {
//            setMeasuredDimension(width, height);
//        } else {
//            if (width < height * mRatioWidth / mRatioHeight) {
//                setMeasuredDimension(width, width * mRatioHeight / mRatioWidth);
//            } else {
//                setMeasuredDimension(height * mRatioWidth / mRatioHeight, height);
//            }
//        }
//    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        Log.i(TAG,"in onTouchEvent");
        try {
            if (mOriginalRect == null) return false;
            float currentFingerSpacing;
            if (event.getPointerCount() == 2) { //Multi touch.
                currentFingerSpacing = getFingerSpacing(event);
                if (fingerSpacing != 0) {
                    if (currentFingerSpacing > fingerSpacing) {
                        Log.i(TAG,"Zoom in detected");
                        zoomLevel += ZOOM_SENSITIVITY;
                    } else if (currentFingerSpacing < fingerSpacing){
                        Log.i(TAG,"Zoom out detected");
                        zoomLevel -= ZOOM_SENSITIVITY;
                    }
                    mCurrentZoomRect = new Rect
                            (mOriginalRect.left+(int)zoomLevel,
                             mOriginalRect.top+(int)zoomLevel,
                                    mOriginalRect.right-(int)zoomLevel,
                                    mOriginalRect.bottom-(int)zoomLevel);

                    //Check the values are valid.
                    if((mCurrentZoomRect.left > mOriginalRect.left) &&
                            (mCurrentZoomRect.width() > Math.floor(mOriginalRect.width()/maximumZoomLevel)) &&
                            (mCurrentZoomRect.height() > Math.floor(mOriginalRect.height()/maximumZoomLevel))){
                        ((CamActivity) mContext).setZoomRect(mCurrentZoomRect);
                        ((CamActivity) mContext).updatePreview();
                    } else {
                        //Don't change the sise because its not valid.
                        mCurrentZoomRect = mOriginalRect;
                    }
                }
                fingerSpacing = currentFingerSpacing;
            } else{

                return true;
            }
        } catch (Exception e){
            e.printStackTrace();
        }
        return true;
    }

    @SuppressWarnings("deprecation")
    private float getFingerSpacing(MotionEvent event) {
        float x = event.getX(0) - event.getX(1);
        float y = event.getY(0) - event.getY(1);
        return (float) Math.sqrt(x * x + y * y);
    }
}
