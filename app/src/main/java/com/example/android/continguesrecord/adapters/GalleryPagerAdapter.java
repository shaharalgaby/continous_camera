package com.example.android.continguesrecord.adapters;

import android.content.Context;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.example.android.continguesrecord.MyVideo;
import com.example.android.continguesrecord.R;
import com.example.android.continguesrecord.listeners.VideoImageClickListener;

import java.io.File;
import java.util.ArrayList;

public class GalleryPagerAdapter extends PagerAdapter {

    private ArrayList<String> images;
    private LayoutInflater layoutInflater;
    private Context context;
    private VideoImageClickListener listener;
    private MyVideo video;

    public GalleryPagerAdapter(Context context, ArrayList<String> images,
                               VideoImageClickListener listener, MyVideo video){
        this.context = context;
        this.images = images;
        layoutInflater = LayoutInflater.from(context);
        this.listener = listener;
        this.video = video;
    }

    /**
     * Return the number of views available.
     */
    @Override
    public int getCount() {
        if(images == null){
            return 0;
        } else {
            return images.size();
        }
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        View imageLayout = layoutInflater.inflate(R.layout.sliding_images, container, false);

        assert imageLayout != null;

        final ImageView imageView = imageLayout.findViewById(R.id.pager_image);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onImageClicked(video);
            }
        });

        Glide.with(context)
                .load(new File(images.get(position)))
                .into(imageView);

        container.addView(imageLayout,0);

        return imageLayout;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view.equals(object);
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View)object);
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
    }

    @Override
    public Parcelable saveState() {
        return null;
    }
}
