package com.example.android.continguesrecord.Utils;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import com.example.android.continguesrecord.DB.VideosViewModel;
import com.example.android.continguesrecord.MyVideo;
import com.example.android.continguesrecord.services.DeleteService;
import com.example.android.continguesrecord.services.MergeVideosService;

import java.util.ArrayList;

public class ServicesUtils {
    private final static String TAG = ServicesUtils.class.getSimpleName();
    private final static int DELETE_SERVICE_ID = 1000;

    public static void deleteFiles(Context context, ArrayList<String> videoPaths,
                                   ArrayList<String> imgPaths, VideosViewModel viewModel, MyVideo video){
        Log.i(TAG,"In deleteFiles method to start the service");

        Intent mServiceIntent = new Intent();
        mServiceIntent.putStringArrayListExtra(CONSTANTS.INTENT_EXTRA_VIDEO_PATH,videoPaths);
        mServiceIntent.putStringArrayListExtra(CONSTANTS.INTENT_EXTRA_IMG_PATH,imgPaths);

        DeleteService.enqueueWork(context,
                DeleteService.class,
                DELETE_SERVICE_ID,
                mServiceIntent);

        if(video!=null) {
            viewModel.delete(video);
        }
    }

    public static void mergeAndSaveToPublicDirectory(Context context, MyVideo video){
        Intent mServiceIntent = new Intent();
        mServiceIntent.putExtra(CONSTANTS.INTENT_EXTRA_VIDEO, video);

        MergeVideosService.enqueueWork(
                context,
                MergeVideosService.class,
                200,
                mServiceIntent);

    }
}
