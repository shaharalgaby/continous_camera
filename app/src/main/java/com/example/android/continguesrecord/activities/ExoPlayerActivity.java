package com.example.android.continguesrecord.activities;

import android.annotation.SuppressLint;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.drawable.AnimatedVectorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;
import com.example.android.continguesrecord.DB.VideosViewModel;
import com.example.android.continguesrecord.MySeekBar;
import com.example.android.continguesrecord.MyVideo;
import com.example.android.continguesrecord.R;
import com.example.android.continguesrecord.Utils.CONSTANTS;
import com.example.android.continguesrecord.Utils.MyAnimationUtils;
import com.example.android.continguesrecord.Utils.ServicesUtils;
import com.example.android.continguesrecord.Utils.VideoUtils;
import com.example.android.continguesrecord.adapters.HorizontalRecyclerImageAdapter;
import com.example.android.continguesrecord.listeners.HorizontalImageInteractions;
import com.example.android.continguesrecord.listeners.OnSwipeTouchListener;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.source.ConcatenatingMediaSource;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout;
import com.google.android.exoplayer2.ui.PlayerControlView;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;

public class ExoPlayerActivity extends AppCompatActivity implements
        View.OnClickListener,
        HorizontalImageInteractions {

    private static final String KEY_PLAY_WHEN_READY = "play_when_ready";
    private static final String KEY_WINDOW = "window";
    private static final String KEY_POSITION = "position";
    private static final int SELECT_SCALE_ANIMATION_TIME = 100;
    private static final int CONTROLLER_TIMEOUT = 5000;
    private static final int CONTROLLER_FF_AND_RW = 3000;
    private static final int CONTROLLER_INFINITE_TIMEOUT = Integer.MAX_VALUE;
    private static final String TAG = ExoPlayerActivity.class.getSimpleName();

    private AnimatedVectorDrawable mPlayToPauseAnim;
    private AnimatedVectorDrawable mPauseToPlayAnim;

    private ImageView mPlayPauseButton;
    private PlayerView playerView;
    private SimpleExoPlayer player;
    private String[] mVideoPaths;
    private MySeekBar mSeekBar;
    private long mTotalDuration;
    private long[] mSubVideosDuration;
    private TextView mCurrentPositionTV;
    private int mCurVideoPlaying;
    private int updateTvCounter = 0;
    private Handler handler;
    private MyVideo video;
    private HorizontalRecyclerImageAdapter mImagesAdapter;
    private RecyclerView mRecyclerView;
    private ArrayList<String> mImagesList;
    private ArrayList<String> mVideosList;
    private VideosViewModel mViewModel;
    private ImageView mDeleteIV;

    private DataSource.Factory mediaDataSourceFactory;
    private DefaultTrackSelector trackSelector;
    private boolean shouldAutoPlay;

    private ProgressBar progressBar;
    private boolean playWhenReady;
    private int currentWindow;
    private long playbackPosition;

    private boolean isPlaying;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exo_player);

        //Set full screen mode
        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
        decorView.setSystemUiVisibility(uiOptions);

        playerView = findViewById(R.id.exo_player);
        mPlayPauseButton = findViewById(R.id.play_pause_button);
        mPlayPauseButton.setOnClickListener(this);
        mSeekBar = findViewById(R.id.seekBar);
        progressBar = findViewById(R.id.progress_bar);
        mCurrentPositionTV = findViewById(R.id.position);
        mDeleteIV = findViewById(R.id.ic_delete_video);
        mCurVideoPlaying = 0;
        handler = new Handler();

        mViewModel = ViewModelProviders.of(this).get(VideosViewModel.class);

        //Some calculation for the videos from the intent.
        Intent intent = getIntent();
        String videoId = intent.getStringExtra(CONSTANTS.INTENT_VIDEO_ID);

        mViewModel.getVideo(Long.parseLong(videoId)).observe
                (this, this::refreshPlayer);

        if (savedInstanceState == null) {
            playWhenReady = true;
            currentWindow = 0;
            playbackPosition = 0;
        } else {
            playWhenReady = savedInstanceState.getBoolean(KEY_PLAY_WHEN_READY);
            currentWindow = savedInstanceState.getInt(KEY_WINDOW);
            playbackPosition = savedInstanceState.getLong(KEY_POSITION);
        }

        shouldAutoPlay = true;
        mediaDataSourceFactory = new DefaultDataSourceFactory
                (this, Util.getUserAgent
                        (this, "CAMI"));

        //Animations
        mPlayToPauseAnim =
                (AnimatedVectorDrawable) getDrawable(R.drawable.avd_play_to_pause);
        mPauseToPlayAnim =
                (AnimatedVectorDrawable) getDrawable(R.drawable.avd_pause_to_play);
        mPlayPauseButton.setImageDrawable(mPauseToPlayAnim);
        MyAnimationUtils.setOnClickReduceAndRegainSize
                (this, findViewById(R.id.player_back_btn));

        setSwipeListener();
    }

    private void refreshPlayer(MyVideo curVideo){
        Log.i(TAG,"refreshPlayer been called" + curVideo.getDateInMills());
        releasePlayer();
        video = curVideo;
        initializePlayer();
    }

    private void initializePlayer() {
        Log.i(TAG,"initializePlayer been called");

        if(video == null){
            return;
        }
        Log.i(TAG,"initializePlayer after the null");

        mVideosList = video.getVideos();
        mImagesList = video.getImages();
        mVideoPaths = arrayListToString(mVideosList);
        calcSubVideosDuration();

        ((TextView) findViewById(R.id.duration)).setText(VideoUtils.buildLengthFormat(mTotalDuration));

        mCurVideoPlaying = 0;
        playWhenReady = true;
        currentWindow = 0;
        playbackPosition = 0;

        initializeSeekBar();
        initializeRecyclerView();

        playerView.requestFocus();

        trackSelector = new DefaultTrackSelector();

        player = ExoPlayerFactory.newSimpleInstance(this, trackSelector);

        playerView.setPlayer(player);
        playerView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FILL);
        playerView.setFastForwardIncrementMs(CONTROLLER_FF_AND_RW);
        playerView.setRewindIncrementMs(CONTROLLER_FF_AND_RW);
        playerView.setControllerVisibilityListener(visibility -> {
            Log.i(TAG,"Controller visibility listener invoked.");
            if(visibility == PlayerControlView.VISIBLE){
                Log.i(TAG,"Controller is now visible");
                showUI();
            } else {
                Log.i(TAG,"Controller is now hidden");
                hideUI();
            }
        });

        player.setPlayWhenReady(shouldAutoPlay);
        player.setVideoScalingMode(C.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING);

        MediaSource[] mediaSources = new MediaSource[mVideoPaths.length];

        for(int i = 0; i< mediaSources.length; i++) {
            mediaSources[i] = new ExtractorMediaSource.Factory(mediaDataSourceFactory)
                    .createMediaSource(Uri.fromFile(new File(mVideoPaths[i])));
        }

        ConcatenatingMediaSource concatenatingMediaSource =
                new ConcatenatingMediaSource(true, mediaSources);

        boolean haveStartPosition = currentWindow != C.INDEX_UNSET;
        if (haveStartPosition) {
            player.seekTo(currentWindow, playbackPosition);
        }

        player.prepare(concatenatingMediaSource, !haveStartPosition, false);
        player.addListener(playerEventListener);
        playerView.setOnClickListener(this);
    }

    Player.EventListener playerEventListener = new Player.EventListener() {

        @Override
        public void onTimelineChanged(Timeline timeline, @Nullable Object manifest, int reason) {
            mSeekBar.setProgress((int)player.getContentPosition());
        }

        @Override
        public void onLoadingChanged(boolean isLoading) {}

        @Override
        public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {

            switch (playbackState) {
                case Player.STATE_IDLE: // The player does not have any media to play yet.
                    isPlaying = false;
                    progressBar.setVisibility(View.VISIBLE);
                    break;
                case Player.STATE_BUFFERING: // The player is buffering (loading the content)
                    isPlaying = false;
                    progressBar.setVisibility(View.VISIBLE);
                    break;
                case Player.STATE_READY: { // The player is able to immediately play
                    Log.i(TAG,"In state ready");
                    Log.i(TAG,"Play when ready is " + playWhenReady );
                    progressBar.setVisibility(View.GONE);
                    if(playWhenReady) {
                        // media actually playing
                        isPlaying = true;
                        changeSeekBar();
                        mPlayPauseButton.setImageDrawable(mPlayToPauseAnim);
                        mPlayToPauseAnim.start();
                    } else {
                        // player paused in any state
                        isPlaying = false;
                        mPlayPauseButton.setImageDrawable(mPauseToPlayAnim);
                        mPauseToPlayAnim.start();
                    }
                    break;
                }
                case Player.STATE_ENDED: // The player has finished playing the media
                    progressBar.setVisibility(View.GONE);
                    player.seekTo(0,0);
                    player.setPlayWhenReady(false);
                    changeSeekBar();
                    break;
            }
        }

        @Override
        public void onPlayerError(ExoPlaybackException error) {}

        @Override
        public void onPositionDiscontinuity(int reason) {
            switch (reason) {
                case Player.DISCONTINUITY_REASON_PERIOD_TRANSITION: {
                    mCurVideoPlaying++;
                    break;
                }
                case Player.DISCONTINUITY_REASON_AD_INSERTION: break;
                case Player.DISCONTINUITY_REASON_INTERNAL: break;
                case Player.DISCONTINUITY_REASON_SEEK_ADJUSTMENT: break;
                case Player.DISCONTINUITY_REASON_SEEK: break;
            }
        }

        @Override
        public void onSeekProcessed() { }
    };

    ItemTouchHelper.Callback itemTouchHelper = new ItemTouchHelper.Callback() {

        private int oldDragPosition = -1;
        private int newPosition = -1;

        private boolean isAskedToDelete;
        private int videoToDelete;

        private float deleteIcLeft;
        private float deleteIcRight;
        private float deleteIcTop;
        private float deleteIcBottom;
        private int[] location = new int[2];

        @Override
        public int getMovementFlags(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
            return makeFlag(ItemTouchHelper.ACTION_STATE_DRAG,
                    ItemTouchHelper.DOWN|ItemTouchHelper.START|ItemTouchHelper.END);
        }

        @Override
        public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
            if(oldDragPosition == -1) {
                oldDragPosition = viewHolder.getAdapterPosition();
            }

            int fromPosition = viewHolder.getAdapterPosition();
            int toPosition = target.getAdapterPosition();
            newPosition = toPosition;

            if (fromPosition < toPosition) {
                for (int i = fromPosition; i < toPosition; i++) {
                    Collections.swap(mImagesList, i, i + 1);
                    Collections.swap(mVideosList, i, i + 1);
                }
            } else {
                for (int i = fromPosition; i > toPosition; i--) {
                    Collections.swap(mImagesList, i, i - 1);
                    Collections.swap(mVideosList, i, i - 1);
                }
            }

            mImagesAdapter.notifyItemMoved(fromPosition,toPosition);
            return true;
        }

        @Override
        public void clearView(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
            Log.i(TAG,"in clearView method");
            super.clearView(recyclerView, viewHolder);

            mDeleteIV.setVisibility(View.GONE);
            mRecyclerView.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;

            if(isAskedToDelete){
                log("Deleting video num " + videoToDelete);
                ArrayList<String> filesToDelete = new ArrayList<>();
                filesToDelete.add(mImagesList.get(videoToDelete));
                filesToDelete.add(mVideosList.get(videoToDelete));
                mImagesList.remove(videoToDelete);
                mVideosList.remove(videoToDelete);
                ServicesUtils.deleteFiles(getBaseContext(),filesToDelete,null,null,null);
            } else {
                View view = viewHolder.itemView;
                ScaleAnimation scaleAnimation = new ScaleAnimation
                        (1.2f,1f,1.2f,1);
                scaleAnimation.setDuration(SELECT_SCALE_ANIMATION_TIME);
                scaleAnimation.setFillAfter(true);
                AlphaAnimation alphaAnimation = new AlphaAnimation(1,CONSTANTS.IMAGE_VIEW_ALPHA);
                alphaAnimation.setDuration(SELECT_SCALE_ANIMATION_TIME);
                alphaAnimation.setFillAfter(true);
                view.startAnimation(scaleAnimation);
            }

            if((oldDragPosition != newPosition) || isAskedToDelete){
                //The user actually moved the video.

                //we will let the animation end and then start the activity again
                //with the new video
                handler.postDelayed(() -> {
                    video.setVideos(mVideosList);
                    video.setImages(mImagesList);
                    mViewModel.update(video);
                    recreate();
                }, SELECT_SCALE_ANIMATION_TIME);
            }
        }

        @Override
        public void onSelectedChanged(RecyclerView.ViewHolder viewHolder, int actionState) {
            super.onSelectedChanged(viewHolder, actionState);

            mRecyclerView.getLayoutParams().height = ViewGroup.LayoutParams.MATCH_PARENT;

            deleteIcLeft = mDeleteIV.getX();
            deleteIcRight = deleteIcLeft+mDeleteIV.getWidth();
            deleteIcTop = mDeleteIV.getY();
            deleteIcBottom = deleteIcTop+ mDeleteIV.getHeight();

            switch(actionState){
                case ItemTouchHelper.ACTION_STATE_DRAG:{
                    mDeleteIV.setVisibility(View.VISIBLE);

                    playerView.setControllerShowTimeoutMs(CONTROLLER_INFINITE_TIMEOUT);

                    View view = viewHolder.itemView;
                    ScaleAnimation scaleAnimation = new ScaleAnimation
                            (1f,1.2f,1f,1.2f);
                    AlphaAnimation alphaAnimation = new AlphaAnimation(CONSTANTS.IMAGE_VIEW_ALPHA,1.0f);

                    AnimationSet set = new AnimationSet(true);
                    set.setFillAfter(true);
                    set.setDuration(SELECT_SCALE_ANIMATION_TIME);
                    set.addAnimation(alphaAnimation);
                    set.addAnimation(scaleAnimation);
                    view.startAnimation(set);

                    break;
                }
                case ItemTouchHelper.ACTION_STATE_IDLE:{
                    playerView.setControllerShowTimeoutMs(CONTROLLER_TIMEOUT);
                    break;
                }
            }
        }

        @Override
        public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder,
                                float dX, float dY, int actionState, boolean isCurrentlyActive) {
            super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);

            //Get the selected item location to see if its on the trash icon.
            View view = viewHolder.itemView;
            view.getLocationOnScreen(location);
            int xLoc = location[0];
            int yLoc = location[1];

            if(isCurrentlyActive) {
                //The user is dragging the image
                if (xLoc <= deleteIcRight &&
                        deleteIcLeft <= xLoc + view.getWidth() &&
                        yLoc <= deleteIcBottom &&
                        deleteIcTop <= yLoc + view.getHeight()) {

                    isAskedToDelete = true;
                    videoToDelete = viewHolder.getAdapterPosition();
                    mDeleteIV.setBackgroundResource(R.drawable.ic_delete_active);

                } else {
                    mDeleteIV.setBackgroundResource(R.drawable.ic_delete_inactive);
                    isAskedToDelete = false;
                }
            }
        }

        @Override
        public boolean isItemViewSwipeEnabled() {
            return false;
        }

        @Override
        public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {}
    };

    @Override
    public void onImageClicked(int position) {
        player.seekTo(position,0);
        mImagesAdapter.setmSelectedIndex(position);
        mImagesAdapter.notifyDataSetChanged();
        mCurVideoPlaying = position;
        changeSeekBar();
    }

    private void initializeRecyclerView(){
        Log.i(TAG,"Initializing RecyclerView with video id: "+video.getDateInMills());
        mRecyclerView = findViewById(R.id.horizontal_images_recycler_view);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager
                (this,LinearLayoutManager.HORIZONTAL,false);

        mImagesAdapter = new HorizontalRecyclerImageAdapter
                (this,mImagesList,this);

        ItemTouchHelper touchHelper = new ItemTouchHelper(itemTouchHelper);

        touchHelper.attachToRecyclerView(mRecyclerView);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mImagesAdapter);
    }

    private void initializeSeekBar(){
        Log.i(TAG,"Initializing the seek bar");
        mSeekBar.setMax((int)mTotalDuration);
        mSeekBar.setDurations(mSubVideosDuration);
        mSeekBar.setTotalDuration(mTotalDuration);

        mSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                if(b) {
                    mCurrentPositionTV.setText(VideoUtils.buildLengthFormat(i));

                    //Here we find the first index that the position is bigger than.
                    int index = floorSearch(mSubVideosDuration,0,
                            mSubVideosDuration.length-1,i);
                    mCurVideoPlaying = index;
                    player.seekTo(mCurVideoPlaying,i-mSubVideosDuration[index]);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });
        changeSeekBar();
    }

    public void changeSeekBar() {

        if(player!=null) {
            playbackPosition = player.getCurrentPosition();
        }

        mSeekBar.setProgress((int)(playbackPosition+
                                mSubVideosDuration[mCurVideoPlaying]));

        if (updateTvCounter == 20) {
            updateTvCounter = 0;
            mCurrentPositionTV.setText
                    (VideoUtils.buildLengthFormat(playbackPosition+
                            mSubVideosDuration[mCurVideoPlaying]));
        }

        if (isPlaying) {
            Runnable runnable = this::changeSeekBar;
            updateTvCounter++;
            handler.postDelayed(runnable, 50);
        }
    }

    void calcSubVideosDuration(){
        long[] mVideosDuration = new long[mVideoPaths.length];
        for (int i = 0; i < mVideoPaths.length; i++) {
            long duration = VideoUtils.getVideoDuration(mVideoPaths[i]);
            mVideosDuration[i] = duration;
            Log.i(TAG, "Duration for the " + i + " video is " + duration);
        }
        mSubVideosDuration = new long[mVideosDuration.length + 1];
        mSubVideosDuration[0] = 0;
        mSubVideosDuration[1] = mVideosDuration[0];
        for (int i = 2; i < mSubVideosDuration.length; i++) {
            mSubVideosDuration[i] = mVideosDuration[i - 1] + mSubVideosDuration[i - 1];
        }
        mTotalDuration = mSubVideosDuration[mSubVideosDuration.length - 1];
    }

    private void releasePlayer() {
        if (player != null) {
            updateStartPosition();
            shouldAutoPlay = player.getPlayWhenReady();
            player.release();
            player = null;
            trackSelector = null;
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        //if (Util.SDK_INT > 23) {
            initializePlayer();
        //}
    }

    @Override
    public void onResume() {
        super.onResume();
        if ( player == null) {
            initializePlayer();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        //if (Util.SDK_INT <= 23) {
            releasePlayer();
        //}
    }

    @Override
    public void onStop() {
        super.onStop();
        //if (Util.SDK_INT > 23) {
            releasePlayer();
        //}
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        updateStartPosition();

        outState.putBoolean(KEY_PLAY_WHEN_READY, playWhenReady);
        outState.putInt(KEY_WINDOW, currentWindow);
        outState.putLong(KEY_POSITION, playbackPosition);
        super.onSaveInstanceState(outState);
    }

    private void updateStartPosition() {
        if(player != null) {
            playbackPosition = player.getCurrentPosition();
            currentWindow = player.getCurrentWindowIndex();
            playWhenReady = player.getPlayWhenReady();
        }
    }

    @Override
    public void onClick(View v) {

        int id = v.getId();

        switch(id){

            case R.id.exo_player:{
                if(playerView.isControllerVisible()){
                    playerView.hideController();
                } else {
                    playerView.showController();
                }
                break;
            }

            case R.id.play_pause_button: {
                Log.i(TAG,"Play pause button pressed with state isPlaying "+isPlaying);
                if(isPlaying){
                    player.setPlayWhenReady(false);
                } else {
                    player.setPlayWhenReady(true);
                }
                break;
            }

            case R.id.player_back_btn: {
                finish();
                break;
            }
        }
    }

    private void showUI(){
        findViewById(R.id.player_back_btn).setVisibility(View.VISIBLE);
        mRecyclerView.setVisibility(View.VISIBLE);
    }

    private void hideUI(){
        findViewById(R.id.player_back_btn).setVisibility(View.GONE);
        mRecyclerView.setVisibility(View.GONE);
    }

    @SuppressLint("ClickableViewAccessibility")
    private void setSwipeListener(){
        playerView.setOnTouchListener(new OnSwipeTouchListener(this){

            public void onSwipeRight() {
                Log.i(TAG, "swipe right detected");
                if(mCurVideoPlaying > 0) {
                    mCurVideoPlaying--;
                    player.seekTo(mCurVideoPlaying,0);
                }
            }

            public void onSwipeLeft() {
                Log.i(TAG,"swipe left detected");
                if(mCurVideoPlaying < mVideoPaths.length-1){
                    mCurVideoPlaying++;
                    player.seekTo(mCurVideoPlaying,0);
                }
            }
        });
    }

    /*
    Util function to find the highers number, which is lower than x in a
    sorted array.
     */
    public int floorSearch(long arr[], int low,
                                       int high, int x){
        // If low and high cross each other
        if (low > high)
            return -1;

        // If last element is smaller than x
        if (x >= arr[high])
            return high;

        // Find the middle point
        int mid = (low+high)/2;

        // If middle point is floor.
        if (arr[mid] == x)
            return mid;

        // If x lies between mid-1 and mid
        if (mid > 0 && arr[mid-1] <= x && x <
                arr[mid])
            return mid-1;

        // If x is smaller than mid, floor
        // must be in left half.
        if (x < arr[mid])
            return floorSearch(arr, low,
                    mid - 1, x);

        // If mid-1 is not floor and x is
        // greater than arr[mid],
        return floorSearch(arr, mid + 1, high,
                x);
    }

    public String[] arrayListToString(ArrayList<String> arrayList){
        String[] array = new String[arrayList.size()];
        for(int i = 0 ; i < arrayList.size(); i++){
            array[i] = arrayList.get(i);
        }

        return array;
    }

    public void log(String s){
        Log.i(TAG,s);
    }

}