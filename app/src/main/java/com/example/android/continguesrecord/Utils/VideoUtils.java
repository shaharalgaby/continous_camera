package com.example.android.continguesrecord.Utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.media.ThumbnailUtils;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.util.Log;
import android.util.Size;

import com.coremedia.iso.IsoFile;
import com.coremedia.iso.boxes.Container;
import com.coremedia.iso.boxes.TimeToSampleBox;
import com.coremedia.iso.boxes.TrackBox;
import com.example.android.continguesrecord.DB.VideosViewModel;
import com.example.android.continguesrecord.MyVideo;
import com.github.hiteshsondhi88.libffmpeg.ExecuteBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.FFmpeg;
import com.googlecode.mp4parser.DataSource;
import com.googlecode.mp4parser.FileDataSourceImpl;
import com.googlecode.mp4parser.authoring.Movie;
import com.googlecode.mp4parser.authoring.Mp4TrackImpl;
import com.googlecode.mp4parser.authoring.builder.DefaultMp4Builder;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

public class VideoUtils {

    private static final String TAG = VideoUtils.class.getSimpleName();

    public static void execFFmpegBinary(final Context context,
                                        final String[] command
                                        ,final ExecuteBinaryResponseHandler handler){
        FFmpeg ffmpeg = FFmpeg.getInstance(context);
        try {
            ffmpeg.execute(command, handler);
        } catch (Exception e) {
            // do nothing for now
            e.printStackTrace();
        }
    }

    public static void createVideoThumbnail
            (final String imgPath,final String videoPath,
             final VideosViewModel viewModel, final MyVideo video){
        AsyncTask createThumbnailTask = new AsyncTask<Object,Void,Void>() {

            @Override
            protected Void doInBackground(Object... objects) {
                Bitmap bMap = ThumbnailUtils.createVideoThumbnail
                        (videoPath,MediaStore.Video.Thumbnails.FULL_SCREEN_KIND);

                File saveToFile = new File(imgPath);
                try {

                    FileOutputStream outputStream = new FileOutputStream(saveToFile);
                    bMap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
                    outputStream.close();

                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                viewModel.update(video);
            }
        };
        createThumbnailTask.execute(new Object());
    }

    /**
     *
     * @param choices The list of available sizes
     * @return The video size
     *     Curretn size is: 3840x2160
    Curretn size is: 2048x1536
    Curretn size is: 1920x1080
    Curretn size is: 1280x720
    Curretn size is: 720x480
    Curretn size is: 640x480
     */
    public static Size chooseVideoSize(Size[] choices) {
        for (Size size : choices) {
            Log.i(TAG,"Curretn size is: "+size);
            if (size.getWidth() == 1920 && size.getHeight() == 1080) {
                return size;
            } else
                if (size.getWidth() == 1280 && size.getHeight() == 720){
                return size;
            } else
                if (size.getWidth() == 720 && size.getHeight() == 480){
                return size;
            }
        }
        Log.e(TAG, "Couldn't find any suitable video size");
        return choices[choices.length - 1];
    }

    /**
     * Given {@code choices} of {@code Size}s supported by a camera, chooses the smallest one whose
     * width and height are at least as large as the respective requested values, and whose aspect
     * ratio matches with the specified value.
     *
     * @param choices     The list of sizes that the camera supports for the intended output class
     * @param width       The minimum desired width
     * @param height      The minimum desired height
     * @param aspectRatio The aspect ratio
     * @return The optimal {@code Size}, or an arbitrary one if none were big enough
     */
    public static Size chooseOptimalSize(Size[] choices, int width, int height, Size aspectRatio) {
        // Collect the supported resolutions that are at least as big as the preview Surface
        List<Size> bigEnough = new ArrayList<>();
        int w = aspectRatio.getWidth();
        int h = aspectRatio.getHeight();
        for (Size option : choices) {
            if (option.getHeight() == option.getWidth() * h / w &&
                    option.getWidth() >= width && option.getHeight() >= height) {
                bigEnough.add(option);
            }
        }

        // Pick the smallest of those, assuming we found any
        if (bigEnough.size() > 0) {
            return Collections.min(bigEnough, new CompareSizesByArea());
        } else {
            Log.e(TAG, "Couldn't find any suitable preview size");
            return choices[0];
        }
    }

    public static long getVideoDuration(String path){
        try {
            FileInputStream fileInputStream = new FileInputStream(new File(path));
            MediaPlayer mediaPlayer = new MediaPlayer();
            mediaPlayer.setDataSource(fileInputStream.getFD());
            fileInputStream.close();
            mediaPlayer.prepare();
            long duration = mediaPlayer.getDuration();
            mediaPlayer.reset();
            mediaPlayer.release();
            return duration;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static String buildLengthFormat(long millis){
        String mill = String.format(Locale.ENGLISH,"%02d",(int) (millis ) % 1000);
        String seconds = String.format(Locale.ENGLISH,"%02d",(int) (millis / 1000) % 60);
        String min = String.format(Locale.ENGLISH,"%02d",(int) ((millis / (1000*60)) % 60));
        String hours = String.format(Locale.ENGLISH,"%02d",(int)(millis)/(1000*60*60));

        return min+":"+seconds;
    }

    public static String parseVideo(String mFilePath,Context context) throws IOException {
        Log.i(TAG,"Start parsing the video");
        DataSource channel = new FileDataSourceImpl(mFilePath);
        IsoFile isoFile = new IsoFile(channel);
        List<TrackBox> trackBoxes = isoFile.getMovieBox().getBoxes(TrackBox.class);
        boolean isError = false;
        for (TrackBox trackBox : trackBoxes) {
            Log.i(TAG,"Checking track " + trackBox);

            TimeToSampleBox.Entry firstEntry = trackBox.getMediaBox()
                    .getMediaInformationBox().getSampleTableBox()
                    .getTimeToSampleBox().getEntries().get(0);
            // Detect if first sample is a problem and fix it in isoFile
            // This is a hack. The audio deltas are 1024 for my files, and video deltas about 3000
            // 10000 seems sufficient since for 30 fps the normal delta is about 3000
            if (firstEntry.getDelta() > 10000) {
                Log.i(TAG,"There is a error in sync");
                isError = true;
                firstEntry.setDelta(3000);
            }
        }

        String filePath = PathUtils.getVideoFilePath(context,System.currentTimeMillis()+"");
        if (isError) {
            Movie movie = new Movie();
            for (TrackBox trackBox : trackBoxes) {
                movie.addTrack(new Mp4TrackImpl(channel.toString() + "[" + trackBox.getTrackHeaderBox().getTrackId() + "]", trackBox));
            }
            movie.setMatrix(isoFile.getMovieBox().getMovieHeaderBox().getMatrix());
            Container out = new DefaultMp4Builder().build(movie);

            //delete file first!
            FileChannel fc = new RandomAccessFile(filePath, "rw").getChannel();
            out.writeContainer(fc);
            fc.close();
            Log.d(TAG, "Finished correcting raw video");
            return filePath;
        }
        return mFilePath;
    }

//    public static String buildLengthForCommands(long millis){
//        String mill = String.format(Locale.ENGLISH,"%03d",(int) (millis ) % 1000);
//        String seconds = String.format(Locale.ENGLISH,"%02d",(int) (millis / 1000) % 60);
//        String min = String.format(Locale.ENGLISH,"%02d",(int) ((millis / (1000*60)) % 60));
//        String hours = String.format(Locale.ENGLISH,"%02d",(int)(millis)/(1000*60*60));
//
//        return hours+":"+min+":"+seconds+"."+mill;
//    }

    /**
     * Compares two {@code Size}s based on their areas.
     */
    public static class CompareSizesByArea implements Comparator<Size> {

        @Override
        public int compare(Size lhs, Size rhs) {
            // We cast here to ensure the multiplications won't overflow
            return Long.signum((long) lhs.getWidth() * lhs.getHeight() -
                    (long) rhs.getWidth() * rhs.getHeight());
        }
    }
}


