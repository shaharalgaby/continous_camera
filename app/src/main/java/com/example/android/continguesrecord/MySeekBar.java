package com.example.android.continguesrecord;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;

public class MySeekBar extends android.support.v7.widget.AppCompatSeekBar {

    public static final String TAG = MySeekBar.class.getSimpleName();

    long[] mSummedDurations;
//    Bitmap mMarksBitmap;
    long mTotalDuration;

    public MySeekBar(Context context) {
        super(context);
    }

    public MySeekBar(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MySeekBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

//    public void setmMarksBitmap(Bitmap mMarksBitmap) {
//        this.mMarksBitmap = mMarksBitmap;
//    }

    public void setDurations(long[] durations) {
        this.mSummedDurations = durations;
        invalidate();
    }

    public void setTotalDuration(long mTotalDuration) {
        this.mTotalDuration = mTotalDuration;
    }

    @Override
    protected synchronized void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        int width = getMeasuredWidth()-getPaddingStart()-getPaddingEnd();
        int start = getPaddingStart();
        int height = getMeasuredHeight();

        Paint paint = new Paint();
        paint.setColor(Color.WHITE);
        paint.setFakeBoldText(true);
        paint.setStrokeWidth(5);

        for (long mSummedDuration : mSummedDurations) {
            long xPosition = start + width * mSummedDuration / mTotalDuration;

            canvas.drawLine(xPosition, getY() - height,
                        xPosition, getY() + height,
                        paint);
        }
    }

}
