package com.example.android.continguesrecord.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.example.android.continguesrecord.DB.VideosViewModel;
import com.example.android.continguesrecord.MyVideo;
import com.example.android.continguesrecord.R;
import com.example.android.continguesrecord.listeners.VideoImageClickListener;
import com.example.android.continguesrecord.Utils.CalendarUtils;
import com.example.android.continguesrecord.Utils.ServicesUtils;
import com.example.android.continguesrecord.Utils.VideoUtils;

import java.util.ArrayList;
import java.util.List;

public class LandActivityAdapter extends
        RecyclerView.Adapter<LandActivityAdapter.LandActivityViewHolder> {

    /*
        In this class we will initialize all the views
     */
    class LandActivityViewHolder extends RecyclerView.ViewHolder {
        private ViewPager mVideoImagePager;
        private ImageView mFlowMenu;
        private TextView mVideoTitle;
        private ProgressBar mProgressBar;
        private TextView mVideoDate;
        private TextView mVideoDuration;

        LandActivityViewHolder(View view){
            super(view);
            mVideoTitle = view.findViewById(R.id.mVideoTitle);
            mFlowMenu = view.findViewById(R.id.overflow);
            mVideoImagePager = view.findViewById(R.id.mVideoImage);
            mProgressBar = view.findViewById(R.id.processBar);
            mVideoDate = view.findViewById(R.id.mVideoDate);
            mVideoDuration = view.findViewById(R.id.mVideoDuration);
        }
    }

    private final VideoImageClickListener imageClickListener;
    private final static String TAG = LandActivityAdapter.class.getSimpleName();
    private final int GAP_BETWEEN_PAGER_IMAGES = 10;

    private ArrayList<MyVideo> mVideosList;
    private final Context mContext;
    private VideosViewModel mViewModel;

    public LandActivityAdapter(Context context, ArrayList<MyVideo> videosList,
                               VideosViewModel viewModel, VideoImageClickListener listener){
        this.mContext = context;
        this.mVideosList = videosList;
        this.mViewModel = viewModel;
        this.imageClickListener = listener;
    }

    @NonNull
    @Override
    public LandActivityViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(mContext)
                .inflate(R.layout.video_thumbnail,parent,false);

        if(view != null){
            view.setFocusable(true);
        }

        return new LandActivityViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final LandActivityViewHolder holder, final int position) {
        //here we should give life to the views
        final MyVideo mVideo = mVideosList.get(position);

        int duration = mVideo.getDuration();
        holder.mVideoDuration.setText(VideoUtils.buildLengthFormat(duration));

        holder.mVideoDate.setText(CalendarUtils.getDateFromMillis(mVideo.getDateInMills()));
        if(mVideo.getTitle().length() > 0){
            holder.mVideoTitle.setText(mVideo.getTitle());
        } else {
            holder.mVideoTitle.setText(R.string.no_title);
        }

        if(mVideo.isProcessing()) {
            holder.mProgressBar.setVisibility(View.VISIBLE);
            holder.mVideoImagePager.setEnabled(true);
            holder.mFlowMenu.setEnabled(true);
        } else {
            holder.mProgressBar.setVisibility(View.GONE);
            holder.mVideoImagePager.setEnabled(true);
            holder.mFlowMenu.setEnabled(true);
        }

        holder.mVideoImagePager.setAdapter
                (new GalleryPagerAdapter(mContext,mVideo.getImages()
                        ,imageClickListener,mVideo));

        int left = GAP_BETWEEN_PAGER_IMAGES;
        int right = GAP_BETWEEN_PAGER_IMAGES;
        holder.mVideoImagePager.setClipToPadding(false);
        holder.mVideoImagePager.setPadding(left,0,right,0);
        holder.mVideoImagePager.setPageTransformer(false,
                new ViewPager.PageTransformer() {
                    @Override
                    public void transformPage(@NonNull View page, float position) {
                        if (holder.mVideoImagePager.getCurrentItem() == 0) {
                            page.setTranslationX(-left);
                        } else if (holder.mVideoImagePager.getCurrentItem() ==
                                holder.mVideoImagePager.getAdapter().getCount() - 1) {
                            page.setTranslationX(right);
                        } else {
                            page.setTranslationX(0);
                        }
                    }
                });

        holder.mFlowMenu.setOnClickListener(view ->
                showPopupMenu(holder.mFlowMenu, holder.getAdapterPosition()));

    }

    /**
     * Showing popup menu when tapping on 3 dots
     */
    private void showPopupMenu(View view, int position) {
        // inflate menu
        PopupMenu popup = new PopupMenu(mContext, view);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.video_menu, popup.getMenu());

        popup.setOnMenuItemClickListener(new MyMenuItemClickListener(position));

        popup.show();
    }

    /**
     * Click listener for popup menu items
     */
    class MyMenuItemClickListener implements PopupMenu.OnMenuItemClickListener {

        private int position;

        private MyMenuItemClickListener(int position) {
            this.position = position;
        }

        @Override
        public boolean onMenuItemClick(MenuItem menuItem) {
            switch (menuItem.getItemId()) {
                case R.id.action_edit: {
                    imageClickListener.onEditClicked(mVideosList.get(position));
                    return true;
                }
                case R.id.action_share: {
                    imageClickListener.onDownloadClicked(mVideosList.get(position));
                    return true;
                }
                case R.id.action_delete: {
                    Log.i(TAG,"Delete video clicked.");
                    removeVideo(position);
                }

                default:
            }
            return false;
        }
    }

    private void removeVideo(int position){
        Log.i(TAG,"Video deleted from position: " + position);

        MyVideo video = mVideosList.get(position);

        ArrayList<String> videoPath = video.getVideos();
        ArrayList<String> imgPath = video.getImages();

        ServicesUtils.deleteFiles(mContext,videoPath,imgPath,mViewModel,video);
    }

    public ArrayList<MyVideo> getVideos(){
        return mVideosList;
    }

    /*
    This method is called every time there is a change in the
    DB by the observer.
     */
    public void setVideos(List<MyVideo> myVideos){

        //an insert action, because the videos DES sorted by date,
        // we always insert to location 0.
//        if(myVideos.size() > mVideosList.size()) {
//            Log.i(TAG, "Add video action detected.");
//            notifyItemInserted(0);
//        }
//
//        //Its a delete action.
//        else if(isDeleteAction){
//            Log.i(TAG, "Delete video action detected, with position: " + deletePosition);
//            notifyItemRemoved(deletePosition);
//            isDeleteAction = false;
//        }
//
//        mVideosList = new ArrayList<>(myVideos);

        mVideosList = new ArrayList<>(myVideos);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        if(mVideosList == null) {
            return 0;
        }
        else {
            return mVideosList.size();
        }
    }
}
