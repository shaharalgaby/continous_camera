package com.example.android.continguesrecord.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.android.continguesrecord.listeners.WheelPickListener;
import com.example.android.continguesrecord.R;

import java.util.ArrayList;

public class WheelAdapter extends RecyclerView.Adapter<WheelAdapter.WheelViewHolder> {

    private final static String TAG = WheelAdapter.class.getSimpleName();
    private final WheelPickListener pickListener;

    private Context mContext;
    private ArrayList<String> mItemList;
    private int type;

    public WheelAdapter(Context context, ArrayList<String> list, WheelPickListener listener,int type){
        this.mContext = context;
        this.mItemList = list;
        this.pickListener = listener;
        this.type = type;
    }

    @NonNull
    @Override
    public WheelViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(mContext)
                .inflate(R.layout.wheel_item,parent,false);

        if(view != null){
            view.setFocusable(true);
        }

        return new WheelViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final @NonNull WheelViewHolder holder, final int position) {
        final String curString = mItemList.get(position);
        holder.mTextView.setText(curString);

        holder.mTextView.setOnClickListener(view -> {
            Log.i(TAG, "Item was chosen");
            pickListener.onItemChosen(curString,holder.getAdapterPosition(), type);
        });
    }


    @Override
    public int getItemCount() {
       if(mItemList == null){
           return 0;
       }
       else {
           return mItemList.size();
       }
    }

    class WheelViewHolder extends RecyclerView.ViewHolder{

        private TextView mTextView;

        WheelViewHolder(final View itemView) {
            super(itemView);
            mTextView = itemView.findViewById(R.id.tv_wheel);
        }
    }
}
