package com.example.android.continguesrecord.Utils;

public class CONSTANTS {

    public final static String INTENT_EXTRA_VIDEO = "mVideo";
    public final static String INTENT_VIDEO_ID = "videoId";

    public static final String TABLE_NAME = "my_videos" ;
    public static final String DATABASE_NAME = "my_videos" ;

    public static final long RECYCLER_MOVE_DURATION = 500;

    public static final String INTENT_EXTRA_VIDEO_PATH = "video_path";
    public static final String INTENT_EXTRA_IMG_PATH = "img_path";

    public static final int WHEEL_TYPE_FILTER = 0;
    public static final int WHEEL_TYPE_TIMER = 1;

    public static final int ORIENTATION_BACK_LANDSCAPE = 0;
    public static final int ORIENTATION_FRONT_LANDSCAPE = 1;
    public static final int ORIENTATION_BACK_PORTRAIT = 2;
    public static final int ORIENTATION_FRONT_PORTRAIT = 3;
    public static final String INTENT_EXTRA_VIDEOS_ARRAY_PATH = "videos_array";
    public static final String EXTRA_EDIT_SLOWMOTION = "slowmotion" ;
    public static final String INTENT_EXTRAS_EDIT_NEEDED = "extra_edit";
    public static final float IMAGE_VIEW_ALPHA = 0.7f;
}
