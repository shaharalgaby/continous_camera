package com.example.android.continguesrecord.listeners;

public interface WheelPickListener {
    void onItemChosen(String txt,int position,int type);
}
