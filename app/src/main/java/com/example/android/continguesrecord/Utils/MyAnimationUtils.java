package com.example.android.continguesrecord.Utils;

import android.content.Context;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;

import com.example.android.continguesrecord.R;

public class MyAnimationUtils {

    public static void setOnClickReduceAndRegainSize(final Context context, View view){
        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {

                if(event.getAction() == MotionEvent.ACTION_DOWN) {
                    Animation reducer = android.view.animation.AnimationUtils
                            .loadAnimation(context, R.anim.reduce_size);
                    reducer.setFillAfter(true);
                    view.startAnimation(reducer);
                }
                else if (event.getAction() == MotionEvent.ACTION_UP) {
                    Animation reducer = android.view.animation.AnimationUtils
                            .loadAnimation(context,R.anim.regain_size);
                    reducer.setFillAfter(true);
                    view.startAnimation(reducer);
                }
                return false;
            }
        });
    }

    public static void setOnClickBounceSize(final Context context, View view){
        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {

                if(event.getAction() == MotionEvent.ACTION_DOWN) {
                    Animation reducer = android.view.animation.AnimationUtils
                            .loadAnimation(context, R.anim.reduce_size);
                    reducer.setFillAfter(true);
                    view.startAnimation(reducer);
                }
                else if (event.getAction() == MotionEvent.ACTION_UP) {
                    Animation resize = android.view.animation.AnimationUtils
                            .loadAnimation(context,R.anim.oversize_and_back);
                    view.startAnimation(resize);
                }
                return false;
            }
        });
    }

    public static void slideInFromBottom(View view){

        ViewGroup.MarginLayoutParams lp = (ViewGroup.MarginLayoutParams)view.getLayoutParams();
        int bottomMargin = lp.bottomMargin;

        TranslateAnimation slideInFromBottomAnim = new TranslateAnimation(
                0,
                0,
                view.getHeight() + bottomMargin ,
                0
        );
        slideInFromBottomAnim.setDuration(500);
        slideInFromBottomAnim.setFillAfter(true);
        view.startAnimation(slideInFromBottomAnim);

    }

    public static void slideOutToBottom(View view){

        ViewGroup.MarginLayoutParams lp = (ViewGroup.MarginLayoutParams)view.getLayoutParams();
        int bottomMargin = lp.bottomMargin;

        TranslateAnimation slideInAnim = new TranslateAnimation(
                0,
                0,
                0,
                view.getHeight()+bottomMargin
        );
        slideInAnim.setDuration(500);
        slideInAnim.setFillAfter(true);

        view.startAnimation(slideInAnim);
    }

    public static void slideInFromLeft(View view){
        ViewGroup.MarginLayoutParams lp = (ViewGroup.MarginLayoutParams)view.getLayoutParams();
        int leftMargin = lp.leftMargin;

        TranslateAnimation slideInAnim = new TranslateAnimation(
                -(view.getWidth()+leftMargin),
                0,
                0,
                0
        );
        slideInAnim.setDuration(500);
        slideInAnim.setFillAfter(true);

        view.startAnimation(slideInAnim);
    }

    public static void slideOutToLeft(View view){
        ViewGroup.MarginLayoutParams lp = (ViewGroup.MarginLayoutParams)view.getLayoutParams();
        int leftMargin = lp.leftMargin;

        TranslateAnimation slideInAnim = new TranslateAnimation(
                0,
                -(view.getWidth()+leftMargin),
                0,
                0
        );
        slideInAnim.setDuration(500);
        slideInAnim.setFillAfter(true);

        view.startAnimation(slideInAnim);
    }

    public static void slideInFromRight(View view){
        ViewGroup.MarginLayoutParams lp = (ViewGroup.MarginLayoutParams)view.getLayoutParams();
        int rightMargin = lp.rightMargin;

        TranslateAnimation slideInAnim = new TranslateAnimation(
                view.getWidth()+rightMargin,
                0,
                0,
                0
        );
        slideInAnim.setDuration(500);
        slideInAnim.setFillAfter(true);

        view.startAnimation(slideInAnim);
    }

    public static void slideOutToRight(View view){
        ViewGroup.MarginLayoutParams lp = (ViewGroup.MarginLayoutParams)view.getLayoutParams();
        int rightMargin = lp.rightMargin;

        TranslateAnimation slideInAnim = new TranslateAnimation(
                0,
                view.getWidth()+rightMargin,
                0,
                0
        );
        slideInAnim.setDuration(500);
        slideInAnim.setFillAfter(true);

        view.startAnimation(slideInAnim);
    }

    public static void growView(View view){
        ScaleAnimation scaleAnimation = new ScaleAnimation(
                0,1,0,1
        );
        scaleAnimation.setDuration(500);
        scaleAnimation.setFillAfter(true);
        scaleAnimation.setStartOffset(500);
        scaleAnimation.setInterpolator(new AccelerateInterpolator(2));

        view.startAnimation(scaleAnimation);
    }
}
