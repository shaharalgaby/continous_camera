package com.example.android.continguesrecord.listeners;

import android.widget.ImageView;

import com.example.android.continguesrecord.MyVideo;

public interface VideoImageClickListener {
    void onImageClicked(MyVideo myVideo);

    void onEditClicked(MyVideo myVideo);

    void onDownloadClicked(MyVideo myVideo);
}
