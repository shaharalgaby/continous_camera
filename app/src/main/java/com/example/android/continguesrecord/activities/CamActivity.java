package com.example.android.continguesrecord.activities;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.SurfaceTexture;
import android.graphics.drawable.AnimatedVectorDrawable;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CameraMetadata;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.HandlerThread;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.Size;
import android.util.SparseIntArray;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.example.android.continguesrecord.AutoFitTextureView;
import com.example.android.continguesrecord.DB.VideosViewModel;
import com.example.android.continguesrecord.listeners.WheelPickListener;
import com.example.android.continguesrecord.services.CropBeginingService;
import com.example.android.continguesrecord.MyVideo;
import com.example.android.continguesrecord.R;
import com.example.android.continguesrecord.Utils.CONSTANTS;
import com.example.android.continguesrecord.Utils.MyAnimationUtils;
import com.example.android.continguesrecord.Utils.PathUtils;
import com.example.android.continguesrecord.Utils.ServicesUtils;
import com.example.android.continguesrecord.Utils.VideoUtils;
import com.example.android.continguesrecord.adapters.WheelAdapter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import travel.ithaka.android.horizontalpickerlib.PickerLayoutManager;

public class CamActivity extends AppCompatActivity implements
        View.OnClickListener,
        View.OnLongClickListener,
        View.OnTouchListener,
        WheelPickListener {

    private static final int FRONT_CAMERA = 1;
    private static final int BACK_CAMERA = 0;
    private static final int DIRECTION_HAVE_NOT_INITIALIZED = -1;

    //Constants
    private static final int SENSOR_ORIENTATION_DEFAULT_DEGREES = 90;
    private static final int SENSOR_ORIENTATION_INVERSE_DEGREES = 270;
    private static final SparseIntArray DEFAULT_ORIENTATIONS = new SparseIntArray();
    private static final SparseIntArray INVERSE_ORIENTATIONS = new SparseIntArray();

    private static final int MERGE_JOB_ID = 1000;

    private static final String TAG = "Camera2VideoFragment";

    private static SparseIntArray ORIENTATIONS = new SparseIntArray();
    static {
        ORIENTATIONS.append(Surface.ROTATION_0, 0);
        ORIENTATIONS.append(Surface.ROTATION_90, 90);
        ORIENTATIONS.append(Surface.ROTATION_180, 180);
        ORIENTATIONS.append(Surface.ROTATION_270, 270);
    }

    static {
        DEFAULT_ORIENTATIONS.append(Surface.ROTATION_0, 90);
        DEFAULT_ORIENTATIONS.append(Surface.ROTATION_90, 0);
        DEFAULT_ORIENTATIONS.append(Surface.ROTATION_180, 270);
        DEFAULT_ORIENTATIONS.append(Surface.ROTATION_270, 180);
    }

    static {
        INVERSE_ORIENTATIONS.append(Surface.ROTATION_0, 270);
        INVERSE_ORIENTATIONS.append(Surface.ROTATION_90, 0);
        INVERSE_ORIENTATIONS.append(Surface.ROTATION_180, 90);
        INVERSE_ORIENTATIONS.append(Surface.ROTATION_270, 180);
    }

    private static final int[] EFFECT_VALUES = {
            CameraMetadata.CONTROL_EFFECT_MODE_OFF,
            CameraMetadata.CONTROL_EFFECT_MODE_MONO,
            CameraMetadata.CONTROL_EFFECT_MODE_NEGATIVE,
            CameraMetadata.CONTROL_EFFECT_MODE_POSTERIZE,
            CameraMetadata.CONTROL_EFFECT_MODE_SEPIA,
            CameraMetadata.CONTROL_EFFECT_MODE_SOLARIZE,
    };
    //Must correspond to effect values.
    private static final String[] EFFECTS_NAMES = {
            "Off",
            "Mono",
            "Negative",
            "Posterize",
            "Sepia",
            "Solarize",
    };
    private static final String[] TIMER_SECONDS = {"Off","1","2","3","4","5","6","7","8","9","10"};

    //For fast < 30, for slow > 30
    //TimeLapse && Slow-mo
    private static final int NORMAL_CAPTURE_RATE = 25;
    int encodingBitrate = 10000000;
    int frameRate = 25;

    //my views and variables.
    private MyVideo mVideo;
    private AutoFitTextureView mTextureView;
    private FrameLayout mButtonVideo;
    private ImageView mButtonFlashIcon;
    private FrameLayout mButtonRotate;
    private FrameLayout mButtonTimer;
    private FrameLayout mButtonFlash;
    private FrameLayout mButtonEffect;
    private String mNextRecordVideoPath;
    private String mNextRecordImagePath;
    private boolean mIsRecordingVideo;
    private boolean mIsPausedVideo;
    private boolean mWasSomethingRecorded;
    private boolean mIsLongPressRecord;
    private boolean mIsFlashOn;
    private boolean mIsTimerOn;
    private int mCameraIndex;
    private int mChosenEffect;
    private int pickerType = -1;
    private int mTimerCountdown;
    private ImageView mViewIsRecording;
    private VideosViewModel mViewModel;
    private Rect mZoomRect;
    private ImageView imageView;
    private AnimatedVectorDrawable recordToPause;
    private AnimatedVectorDrawable pauseToRecord;
    private AnimatedVectorDrawable recordingAnimation;
    private LinearLayout mSlideMenu;
    private RecyclerView mRecyclerView;
    private PickerLayoutManager wheelLayoutManager;
    private TextView tvCountdown;

    //camera related variables.
    private CameraDevice mCameraDevice; //A reference to the opened CameraDevice
    private CameraCaptureSession mPreviewSession; //A reference to the current session for preview
    private Size mPreviewSize; //The size of camera preview
    private Size mVideoSize; // The size of video recording
    private MediaRecorder mMediaRecorder; // The media recorder
    private Integer mSensorOrientation; //Landscape or portrait
    private CaptureRequest.Builder mPreviewBuilder;

    private HandlerThread mBackgroundThread; //An additional thread for running tasks that shouldn't block the UI.
    private Handler mBackgroundHandler; //A  handler for running tasks in the background.

    // A semaphore to prevent the app from exiting before closing the camera.
    private Semaphore mCameraOpenCloseLock = new Semaphore(1);


    /**
     * {@link TextureView.SurfaceTextureListener} handles several lifecycle events on a
     * {@link TextureView}.
     */
    private TextureView.SurfaceTextureListener mSurfaceTextureListener
            = new TextureView.SurfaceTextureListener() {

        @Override
        public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture,
                                              int width, int height) {
            openCamera(width, height);
        }

        @Override
        public void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture,
                                                int width, int height) {
            configureTransform(width, height);
        }

        @Override
        public boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture) {
            return true;
        }

        @Override
        public void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture) {}

    };

    /**
     * this is called when CameraDevice changes its status.
     */
    private CameraDevice.StateCallback mStateCallback = new CameraDevice.StateCallback() {

        @Override
        public void onOpened(@NonNull CameraDevice cameraDevice) {
            mCameraDevice = cameraDevice;
            startPreview();
            mCameraOpenCloseLock.release();
            if (null != mTextureView) {
                configureTransform(mTextureView.getWidth(), mTextureView.getHeight());
            }
            //We start recording as soon as the camera is ready, and then pausing the video,
            // So the user will get a smooth start record experience.
            startRecordingVideo();
            showViews();
        }

        @Override
        public void onDisconnected(@NonNull CameraDevice cameraDevice) {
            mCameraOpenCloseLock.release();
            cameraDevice.close();
            mCameraDevice = null;
        }

        @Override
        public void onError(@NonNull CameraDevice cameraDevice, int error) {
            mCameraOpenCloseLock.release();
            cameraDevice.close();
            mCameraDevice = null;
            finish();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG, "In onCreate method");
        setContentView(R.layout.activity_cam);

        //setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LOCKED);

        //Set full screen mode
        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
        decorView.setSystemUiVisibility(uiOptions);

        //Initialize variables
        mWasSomethingRecorded = false;
        mIsRecordingVideo = false;
        mIsLongPressRecord = false;
        mIsPausedVideo = false;
        mIsFlashOn = false;
        mCameraIndex = 0;
        mChosenEffect = 0;
        mViewModel = ViewModelProviders.of(this).get(VideosViewModel.class);

        //Initialize views
        mTextureView = findViewById(R.id.texture);
        mButtonVideo = findViewById(R.id.video);
        mViewIsRecording = findViewById(R.id.is_recording);
        mButtonFlash = findViewById(R.id.flash_btn);
        mButtonFlashIcon = findViewById(R.id.flash_btn_ic);
        mButtonRotate = findViewById(R.id.camera_rotate);
        mButtonEffect = findViewById(R.id.camera_effect_btn);
        mButtonTimer = findViewById(R.id.camera_timer);
        imageView = findViewById(R.id.imageView);
        mSlideMenu = findViewById(R.id.camera_actions_menu);
        mRecyclerView = findViewById(R.id.wheel_recycle);
        tvCountdown = findViewById(R.id.tv_timer_countdown);

        mButtonVideo.setOnClickListener(this);

        //Initialize the wheel picker
        wheelLayoutManager = new PickerLayoutManager
                (this,PickerLayoutManager.HORIZONTAL,false);
        wheelLayoutManager.setOnScrollStopListener(onScrollStopListener);
        LinearSnapHelper snapHelper = new LinearSnapHelper();
        snapHelper.attachToRecyclerView(mRecyclerView);

        //Get the video from the intent and do find out if its a new video or
        //continued one.
        Intent intent = getIntent();
        mVideo = intent.getParcelableExtra(CONSTANTS.INTENT_EXTRA_VIDEO);

        //Animations initialize
        initAnimations();
    }

    @Override
    public void onResume() {
        Log.i(TAG, "In onResume");
        super.onResume();
        startBackgroundThread();
        if (mTextureView.isAvailable()) {
            Log.i(TAG, "Texture was already available in onResume, opening camera.");
            openCamera(mTextureView.getWidth(), mTextureView.getHeight());
        } else {
            mTextureView.setSurfaceTextureListener(mSurfaceTextureListener);
        }
        if(mVideo.getVideos().size() > 0){
            Log.i(TAG,"video direction is: " + mVideo.getCameraDirection());
            if(mVideo.getCameraOrientation() == Configuration.ORIENTATION_PORTRAIT)
                Log.i(TAG,"video orientation is : Portrait ");
            else
                Log.i(TAG,"video orientation is : Landscape ");

        }
    }

    @Override
    public void onPause() {
        Log.i(TAG, "In onPause");
        saveCurrentSession();
        closeCamera();
        stopBackgroundThread();
        super.onPause();
    }

    private void forceOrientationAndDirection(){
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LOCKED);
    }

    public void initAnimations(){
        recordToPause =
                (AnimatedVectorDrawable)getDrawable(R.drawable.avd_record_to_pause);
        imageView.setImageDrawable(recordToPause);
        pauseToRecord =
                (AnimatedVectorDrawable)getDrawable(R.drawable.avd_pause_to_record);
        recordingAnimation =
                (AnimatedVectorDrawable)getDrawable(R.drawable.avd_recording_animation);
        mViewIsRecording.setImageDrawable(recordingAnimation);

        MyAnimationUtils.setOnClickBounceSize(this, mButtonFlash);
        MyAnimationUtils.setOnClickBounceSize(this, mButtonEffect);
        MyAnimationUtils.setOnClickBounceSize(this,mButtonRotate);
        MyAnimationUtils.setOnClickBounceSize(this,mButtonTimer);
        MyAnimationUtils.setOnClickReduceAndRegainSize(this,mButtonVideo);
    }

    public void initializePickerWheel(int type){
        pickerType = type;
        Log.i(TAG,"Starting picker wheel of type: "+type);
        ArrayList<String> list = null;
        if(type == CONSTANTS.WHEEL_TYPE_FILTER) {
            list = new ArrayList<>(Arrays.asList(EFFECTS_NAMES));
            wheelLayoutManager.setScaleDownBy(0.8f);
            wheelLayoutManager.setScaleDownDistance(0.9f);
        } else if (type == CONSTANTS.WHEEL_TYPE_TIMER){
            list = new ArrayList<>(Arrays.asList(TIMER_SECONDS));
            wheelLayoutManager.setScaleDownBy(0.99f);
            wheelLayoutManager.setScaleDownDistance(0.8f);
        }

        WheelAdapter mWheelAdapter = new WheelAdapter
                (this, list, this, type);

        wheelLayoutManager.setChangeAlpha(true);

        mRecyclerView.setAdapter(mWheelAdapter);
        mRecyclerView.setLayoutManager(wheelLayoutManager);
        mRecyclerView.setVisibility(View.VISIBLE);
        mRecyclerView.smoothScrollBy(20, 0);

        if(type == CONSTANTS.WHEEL_TYPE_FILTER && mChosenEffect > 0){
            Log.i(TAG,"trying to scroll to position "+mChosenEffect);
            mRecyclerView.smoothScrollToPosition(mChosenEffect-1);
        } else if(type == CONSTANTS.WHEEL_TYPE_TIMER && mTimerCountdown > 0){
            mRecyclerView.smoothScrollToPosition(mTimerCountdown-1);
        }
    }

    PickerLayoutManager.onScrollStopListener onScrollStopListener =
            new PickerLayoutManager.onScrollStopListener() {
                @Override
                public void selectedView(View view) {
                    if (view != null) {
                        int position = wheelLayoutManager.getPosition(view);
                        Log.i(TAG, "picker scroll stopped");

                        if (pickerType == CONSTANTS.WHEEL_TYPE_FILTER) {
                            Log.i(TAG, "picker stop on position + " + position);
                            mChosenEffect = position;
                            updatePreview();
                        } else if (pickerType == CONSTANTS.WHEEL_TYPE_TIMER) {
                            Log.i(TAG, "setting up timer");
                            //No need to implement
                            if (position == 0) {
                                mIsTimerOn = false;
                            } else {
                                mIsTimerOn = true;
                                mTimerCountdown = position;
                            }
                        }
                    }
                }
            };

    @Override
    public void onItemChosen(String txt, int position, int type) {
        switch (type){
            case CONSTANTS.WHEEL_TYPE_FILTER:{
                Log.i(TAG,"Item for filter has been chosen");
                mChosenEffect = position;
                updatePreview();
                break;
            }
            case CONSTANTS.WHEEL_TYPE_TIMER:{
                break;
            }
        }
        mRecyclerView.setVisibility(View.GONE);
    }

    public void slideOutMenu(){
        MyAnimationUtils.slideOutToRight(mSlideMenu);
    }

    /**
     * Tries to open a {@link CameraDevice}. The result is listened by `mStateCallback`.
     */
    @SuppressWarnings("MissingPermission")
    private void openCamera(int width, int height) {
        Log.i(TAG, "in OpenCamera");

        CameraManager mCameraManager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
        try {
            Log.d(TAG, "tryAcquire");
            if (!mCameraOpenCloseLock.tryAcquire(2500, TimeUnit.MILLISECONDS)) {
                throw new RuntimeException("Time out waiting to lock camera opening.");
            }
            assert mCameraManager != null;
            String mCameraId = mCameraManager.getCameraIdList()[mCameraIndex];

            // Choose the sizes for camera preview and video recording
            CameraCharacteristics characteristics = mCameraManager.getCameraCharacteristics(mCameraId);
            //int[] availableEffects = characteristics.get(CameraCharacteristics.CONTROL_AVAILABLE_EFFECTS);
            StreamConfigurationMap map = characteristics
                    .get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
            mSensorOrientation = characteristics.get(CameraCharacteristics.SENSOR_ORIENTATION);
            if (map == null) {
                throw new RuntimeException("Cannot get available preview/video sizes");
            }

            mZoomRect = characteristics.get(CameraCharacteristics.SENSOR_INFO_ACTIVE_ARRAY_SIZE);
            if(mTextureView != null) {
                mTextureView.setZoomRect(mZoomRect);
                mTextureView.setMaxZoomLevel
                        (characteristics.get(CameraCharacteristics.SCALER_AVAILABLE_MAX_DIGITAL_ZOOM));
                mTextureView.setContext(this);
            }

            mVideoSize = VideoUtils.chooseVideoSize(map.getOutputSizes(MediaRecorder.class));
            mPreviewSize = VideoUtils.chooseOptimalSize(map.getOutputSizes(SurfaceTexture.class),
                    width, height,mVideoSize);

            Log.i(TAG,"Screen size is: "+width+":"+height);
            Log.i(TAG,"mVideoSize is: "+ mVideoSize.getWidth()+":"+mVideoSize.getHeight());
            Log.i(TAG,"mPreviewSize is: "+mPreviewSize.getWidth()+":"+mPreviewSize.getHeight());

            int orientation = getResources().getConfiguration().orientation;
            if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
                mTextureView.setAspectRatio(mPreviewSize.getWidth(), mPreviewSize.getHeight());
            } else {
                mTextureView.setAspectRatio(mPreviewSize.getHeight(), mPreviewSize.getWidth());
            }

            mMediaRecorder = new MediaRecorder();
            configureTransform(width, height);
            mCameraManager.openCamera(mCameraId, mStateCallback, null);

        } catch (CameraAccessException e) {
            Toast.makeText(this, "Cannot access the camera.", Toast.LENGTH_SHORT).show();
            finish();
        } catch (NullPointerException e) {
            // Currently an NPE is thrown when the Camera2API is used but not supported on the
            // device this code runs.
        } catch (InterruptedException e) {
            throw new RuntimeException("Interrupted while trying to lock camera opening.");
        }
    }

    /**
     * Start the camera preview.
     */
    private void startPreview() {
        if (null == mCameraDevice || !mTextureView.isAvailable() || null == mPreviewSize) {
            return;
        }
        try {
            closePreviewSession();
            SurfaceTexture texture = mTextureView.getSurfaceTexture();
            assert texture != null;
            texture.setDefaultBufferSize(mPreviewSize.getWidth(), mPreviewSize.getHeight());
            mPreviewBuilder = mCameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);

            Surface previewSurface = new Surface(texture);
            mPreviewBuilder.addTarget(previewSurface);

            mCameraDevice.createCaptureSession(Collections.singletonList(previewSurface),
                    new CameraCaptureSession.StateCallback() {

                        @Override
                        public void onConfigured(@NonNull CameraCaptureSession session) {
                            mPreviewSession = session;
                            updatePreview();
                        }

                        @Override
                        public void onConfigureFailed(@NonNull CameraCaptureSession session) {
                            Toast.makeText(CamActivity.this, "Failed", Toast.LENGTH_SHORT).show();
                        }
                    }, mBackgroundHandler);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    /**
     * Update the camera preview. {@link #startPreview()} needs to be called in advance.
     */
    public void updatePreview() {
        if (null == mCameraDevice ) {
            return;
        }
        try {
            setUpCaptureRequestBuilder(mPreviewBuilder);
//            HandlerThread thread = new HandlerThread("CameraPreview");
//            thread.start();
            mPreviewSession.setRepeatingRequest(mPreviewBuilder.build(), null, mBackgroundHandler);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    private void setUpCaptureRequestBuilder(CaptureRequest.Builder builder) {

        Log.i(TAG,"updating the preview.");

        builder.set(CaptureRequest.CONTROL_MODE, CameraMetadata.CONTROL_MODE_AUTO);

        builder.set(CaptureRequest.FLASH_MODE, mIsFlashOn ?
                CameraMetadata.FLASH_MODE_TORCH : CameraMetadata.FLASH_MODE_OFF);

        //This is the way to create camera effects.
        builder.set(CaptureRequest.CONTROL_EFFECT_MODE, EFFECT_VALUES[mChosenEffect]);

        //Set up the zoom
        builder.set(CaptureRequest.SCALER_CROP_REGION, mZoomRect);

    }

    private void closeCamera() {
        try {
            mCameraOpenCloseLock.acquire();
            closePreviewSession();
            if (null != mCameraDevice) {
                mCameraDevice.close();
                mCameraDevice = null;
            }
            if (null != mMediaRecorder) {
                mMediaRecorder.release();
                mMediaRecorder = null;
            }
        } catch (InterruptedException e) {
            throw new RuntimeException("Interrupted while trying to lock camera closing.");
        } finally {
            mCameraOpenCloseLock.release();
        }
    }

    @Override
    public void onClick(View view) {

        Log.i(TAG, "In onClick method");

        switch (view.getId()) {

            case R.id.imageView: {
                runOnUiThread(() -> {
                    if(mIsRecordingVideo) {
                        pauseVideo();
                    } else {
                        mWasSomethingRecorded = true;
                        if(!mIsTimerOn) {
                            resumeVideo();
                        } else {
                            startDelayedRecord();
                        }
                    }
                });
                Log.i(TAG,"Animation button clicked");
                break;
            }

            case R.id.video: {
                Log.i(TAG, "Stop button pressed");
                finish();
                break;
            }

            case R.id.flash_btn: {
                Log.i(TAG, "flash button pressed");
                if (mIsFlashOn)
                    turnTorchOff();
                else
                    turnTorchOn();
                break;
            }

            case R.id.camera_rotate: {
                findViewById(R.id.camera_overlay).setVisibility(View.VISIBLE);
                Log.i(TAG, "Rotate button pressed");
                rotateCamera();
                break;
            }

            case R.id.camera_effect_btn: {
                Log.i(TAG, "Effect button pressed");
                cameraEffect();
                break;
            }

            case R.id.camera_timer: {
                Log.i(TAG, "Timer button pressed");
                if(mRecyclerView.getVisibility() == View.VISIBLE){
                    mRecyclerView.setVisibility(View.GONE);
                } else {
                    initializePickerWheel(CONSTANTS.WHEEL_TYPE_TIMER);
                }
                break;
            }
        }
    }

    @Override
    public boolean onLongClick(View view) {
        Log.i(TAG, "In onLongClick method");

        int id = view.getId();

        switch (id) {
            case (R.id.video): {

                if (mIsPausedVideo) {
                    resumeVideo();
                } else if (!mIsRecordingVideo) {
                    startRecordingVideo();
                }

                break;
            }
        }

        mIsLongPressRecord = true;
        return true;
    }

    @Override
    public boolean onTouch(View view, MotionEvent event) {
        Log.i(TAG, "In onTouch method");
        view.onTouchEvent(event);

        int id = view.getId();

        switch (id) {
            case (R.id.video): {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (mIsLongPressRecord) {
                        Log.i(TAG, "Recognized user release record button");
                        //Do something when user release the record button.
                        pauseVideo();
                        mIsLongPressRecord = false;
                    }
                }
                break;
            }
        }
        return true;
    }

    public void showViews(){
        findViewById(R.id.camera_overlay).setVisibility(View.GONE);

        MyAnimationUtils.slideInFromLeft(mButtonVideo);

        mSlideMenu.setVisibility(View.VISIBLE);
        MyAnimationUtils.slideInFromRight(mSlideMenu);

        imageView.setVisibility(View.VISIBLE);

        mButtonVideo.setVisibility(View.VISIBLE);
        MyAnimationUtils.slideInFromLeft(mButtonVideo);

        MyAnimationUtils.growView(imageView);
    }

    /**
     * Starts a background thread and its {@link Handler}.
     */
    private void startBackgroundThread() {
        mBackgroundThread = new HandlerThread("CameraBackground");
        mBackgroundThread.start();
        mBackgroundHandler = new Handler(mBackgroundThread.getLooper());
    }

    /**
     * Stops the background thread and its {@link Handler}.
     */
    private void stopBackgroundThread() {
        mBackgroundThread.quitSafely();
        try {
            mBackgroundThread.join();
            mBackgroundThread = null;
            mBackgroundHandler = null;
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void startDelayedRecord(){
        tvCountdown.setVisibility(View.VISIBLE);
        CountDownTimer timer = new CountDownTimer
                (mTimerCountdown*1000,500) {
            @Override
            public void onTick(long l) {
                tvCountdown.setText((l/1000+1) + "");
            }

            @Override
            public void onFinish() {
                resumeVideo();
                tvCountdown.setVisibility(View.GONE);
            }
        };
        timer.start();
    }

    private void turnTorchOn() {
        Log.i(TAG, "try to turn torch on");
        mIsFlashOn = true;
        updatePreview();
        mButtonFlashIcon.setImageResource(R.drawable.ic_flash_on);
    }

    private void turnTorchOff() {
        Log.i(TAG, "try to turn torch off");
        mIsFlashOn = false;
        updatePreview();
        mButtonFlashIcon.setImageResource(R.drawable.ic_flash_off);
    }

    private void rotateCamera() {
        Handler handler = new Handler();
        handler.post(() -> {
            mCameraIndex = mCameraIndex == FRONT_CAMERA ? BACK_CAMERA : FRONT_CAMERA;
            closeCamera();
            openCamera(mTextureView.getWidth(), mTextureView.getHeight());
            startRecordingVideo();
        });
    }

    private void cameraEffect(){
        if(mRecyclerView.getVisibility() == View.VISIBLE){
            mRecyclerView.setVisibility(View.GONE);
        } else {
            initializePickerWheel(CONSTANTS.WHEEL_TYPE_FILTER);
        }
    }

    public void setZoomRect(Rect rect){
        this.mZoomRect = rect;
    }

    private void configureTransform(int viewWidth, int viewHeight) {
        if (null == mTextureView || null == mPreviewSize) {
            return;
        }
        int rotation = getWindowManager().getDefaultDisplay().getRotation();
        Matrix matrix = new Matrix();
        RectF viewRect = new RectF(0, 0, viewWidth, viewHeight);
        RectF bufferRect = new RectF(0, 0, mPreviewSize.getHeight(), mPreviewSize.getWidth());
        float centerX = viewRect.centerX();
        float centerY = viewRect.centerY();
        if (Surface.ROTATION_90 == rotation || Surface.ROTATION_270 == rotation) {
            bufferRect.offset(centerX - bufferRect.centerX(), centerY - bufferRect.centerY());
            matrix.setRectToRect(viewRect, bufferRect, Matrix.ScaleToFit.FILL);
            float bounce = Math.max(
                    (float) viewHeight / mPreviewSize.getHeight(),
                    (float) viewWidth / mPreviewSize.getWidth());
            matrix.postScale(bounce, bounce, centerX, centerY);
            matrix.postRotate(90 * (rotation - 2), centerX, centerY);
        }
        mTextureView.setTransform(matrix);
    }

    private void setUpMediaRecorder() throws IOException {
        Log.i(TAG,"Setting up the MediaRecorder");
        mMediaRecorder.setAudioSource(MediaRecorder.AudioSource.CAMCORDER);
        mMediaRecorder.setVideoSource(MediaRecorder.VideoSource.SURFACE);
        String millis = System.currentTimeMillis()+"";
        mNextRecordVideoPath = PathUtils.getVideoFilePath(this,millis);
        mNextRecordImagePath = PathUtils.createImagePath(this, millis);
        mMediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        mMediaRecorder.setVideoEncodingBitRate(encodingBitrate);
        mMediaRecorder.setVideoFrameRate(frameRate);
        //mMediaRecorder.setCaptureRate(NORMAL_CAPTURE_RATE);

        Log.i(TAG,"Setting width and height: "+
                mVideoSize.getWidth()+":"+mVideoSize.getHeight());
        mMediaRecorder.setVideoSize(mVideoSize.getWidth(), mVideoSize.getHeight());
        mMediaRecorder.setVideoEncoder(MediaRecorder.VideoEncoder.H264);
        mMediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
        mMediaRecorder.setAudioEncodingBitRate(encodingBitrate);

        mMediaRecorder.setOutputFile(mNextRecordVideoPath);

        int rotation = getWindowManager().getDefaultDisplay().getRotation();

        log("********** rotation value is: " +rotation +" ***********");
        switch (mSensorOrientation) {
            case SENSOR_ORIENTATION_DEFAULT_DEGREES:
                log("**********Default degrees**********");
                mMediaRecorder.setOrientationHint(DEFAULT_ORIENTATIONS.get(rotation));
                break;
            case SENSOR_ORIENTATION_INVERSE_DEGREES:
                log("**********Inverse degrees**********");
                mMediaRecorder.setOrientationHint(INVERSE_ORIENTATIONS.get(rotation));
                break;
        }

        mMediaRecorder.prepare();
    }

    private void startRecordingVideo() {
        Log.i(TAG,"In startRecordingVideo Method");
        if (null == mCameraDevice || !mTextureView.isAvailable() || null == mPreviewSize) {
            return;
        }
        try {
            setUpMediaRecorder();
            SurfaceTexture texture = mTextureView.getSurfaceTexture();
            assert texture != null;
            texture.setDefaultBufferSize(mPreviewSize.getWidth(), mPreviewSize.getHeight());
            mPreviewBuilder = mCameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_RECORD);
            List<Surface> surfaces = new ArrayList<>();

            // Set up Surface for the camera preview
            Surface previewSurface = new Surface(texture);
            surfaces.add(previewSurface);
            mPreviewBuilder.addTarget(previewSurface);

            // Set up Surface for the MediaRecorder
            Surface recorderSurface = mMediaRecorder.getSurface();
            surfaces.add(recorderSurface);
            mPreviewBuilder.addTarget(recorderSurface);

            // Start a capture session
            // Once the session starts, we can update the UI and start recording
            mCameraDevice.createCaptureSession(surfaces, new CameraCaptureSession.StateCallback() {

                @Override
                public void onConfigured(@NonNull CameraCaptureSession cameraCaptureSession) {
                    Log.i(TAG,"On configued callback has called");
                    mPreviewSession = cameraCaptureSession;
                    updatePreview();
                    runOnUiThread(() -> {
                        Log.i(TAG,"In Run, call start record to Start recording the video.");
                        startRecord();
                    });
                }

                @Override
                public void onConfigureFailed(@NonNull CameraCaptureSession cameraCaptureSession) {
                    Toast.makeText(CamActivity.this, "Failed", Toast.LENGTH_SHORT).show();
                }
            }, mBackgroundHandler);
        } catch (CameraAccessException | IOException e) {
            e.printStackTrace();
        }
    }

    private void startRecord(){
        Log.i(TAG,"In StartRecord method");
        mIsRecordingVideo = true;
        mIsPausedVideo = false;

        // Start recording
        if(mMediaRecorder != null) {
            mMediaRecorder.start();
            pauseVideo();
        }
    }

    private void saveCurrentSession() {
        pauseVideo();
        if(mWasSomethingRecorded) {
            mMediaRecorder.stop();
        }
        Handler handler = new Handler();
        handler.post(() -> {

            if(mWasSomethingRecorded) {
                mVideo.addPath(mNextRecordVideoPath,mNextRecordImagePath);
                //Logic
                if (mVideo.isNewVideo()) {
                    mVideo.setNewVideo(false);
                }
                VideoUtils.createVideoThumbnail(
                        mNextRecordImagePath, mNextRecordVideoPath,mViewModel,mVideo);
                mVideo.setProcessing(true);
                mViewModel.update(mVideo);

                //Doesn't have to be called here.
                cropAndSave();
            } else {
                //nothing was recorded so we will delete the files created.
                Log.i(TAG, "Nothing was recorded, start deleting files");
                ArrayList<String> deleteFiles = new ArrayList<>();
                deleteFiles.add(mNextRecordVideoPath);
                deleteFiles.add(mNextRecordImagePath);
                //If its not a new video we don't want to delete the
                //actual video object.
                if(!mVideo.isNewVideo())
                    mVideo = null;
                ServicesUtils.deleteFiles
                        (CamActivity.this, deleteFiles , new ArrayList<>(),
                                mViewModel, mVideo);

            }
        });

        super.finish();
    }

    private void cropAndSave(){

        Intent mServiceIntent = new Intent();
        mServiceIntent.putExtra(CONSTANTS.INTENT_EXTRA_VIDEO_PATH, mNextRecordVideoPath);
        mServiceIntent.putExtra(CONSTANTS.INTENT_EXTRA_VIDEO,mVideo);

        CropBeginingService.enqueueWork(this,
                CropBeginingService.class,
                MERGE_JOB_ID,
                mServiceIntent);
    }

    private void pauseVideo() {
        mMediaRecorder.pause();
        Log.i(TAG,"Pausing video");

        mIsPausedVideo = true;
        mIsRecordingVideo = false;

        //Take care of UI
        MyAnimationUtils.slideInFromRight(mSlideMenu);
        MyAnimationUtils.slideInFromLeft(mButtonVideo);
        imageView.setImageDrawable(pauseToRecord);
        pauseToRecord.start();
        recordingAnimation.stop();
        mViewIsRecording.setVisibility(View.GONE);
    }

    public void resumeVideo(){
        mMediaRecorder.resume();
        Log.i(TAG,"Resuming video");

        mWasSomethingRecorded = true;
        mIsPausedVideo = false;
        mIsRecordingVideo = true;

        //If its a new video we need to set its orientation and direction.
        if(mVideo.isNewVideo() ){
            mVideo.setNewVideo(false);
            Configuration configuration = getResources().getConfiguration();
            mVideo.setCameraOrientation(configuration.orientation ==
                    Configuration.ORIENTATION_LANDSCAPE ? Configuration.ORIENTATION_LANDSCAPE
                    : Configuration.ORIENTATION_PORTRAIT);
            mVideo.setCameraDirection(mCameraIndex);
            mViewModel.update(mVideo);
            forceOrientationAndDirection();
        }

        //take care of UI.
        mViewIsRecording.setVisibility(View.VISIBLE);
        slideOutMenu();
        MyAnimationUtils.slideOutToLeft(mButtonVideo);
        imageView.setImageDrawable(recordToPause);
        recordToPause.start();
        recordingAnimation.start();
        mRecyclerView.setVisibility(View.GONE);
        mViewIsRecording.setVisibility(View.VISIBLE);
    }

    private void closePreviewSession() {
        if (mPreviewSession != null) {
            mPreviewSession.close();
            mPreviewSession = null;
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        log("Configuration changed!");
        mMediaRecorder.release();
        mMediaRecorder = new MediaRecorder();
        startRecordingVideo();
    }

    public void log(String s) {
        Log.i(TAG,s);
    }
}
