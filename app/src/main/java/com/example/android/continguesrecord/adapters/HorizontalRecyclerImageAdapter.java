package com.example.android.continguesrecord.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.bumptech.glide.Glide;
import com.example.android.continguesrecord.R;
import com.example.android.continguesrecord.Utils.CONSTANTS;
import com.example.android.continguesrecord.listeners.HorizontalImageInteractions;
import com.google.android.exoplayer2.ui.PlayerControlView;
import com.makeramen.roundedimageview.RoundedImageView;

import java.io.File;
import java.util.ArrayList;

public class HorizontalRecyclerImageAdapter extends RecyclerView.Adapter<HorizontalRecyclerImageAdapter.HorizontalViewHolder> {

    public static final String TAG = HorizontalRecyclerImageAdapter.class.getSimpleName();
    private static final int SELECTED_ITEM = 1;
    private static final int NORMAL_ITEM = 0;

    private Context mContext;
    private ArrayList<String> mImageList;
    private HorizontalImageInteractions mHorizontalImageListener;
    private int mSelectedIndex;

    public HorizontalRecyclerImageAdapter(Context context, ArrayList<String> imageList,
                                          HorizontalImageInteractions listener){
        this.mContext = context;
        this.mImageList = imageList;
        this.mHorizontalImageListener = listener;
        this.mSelectedIndex = -1;
    }

    public void setmSelectedIndex(int selectedIndex){
        this.mSelectedIndex = selectedIndex;
    }

    public int getmSelectedIndex(){
        return mSelectedIndex;
    }

    @NonNull
    @Override
    public HorizontalRecyclerImageAdapter.HorizontalViewHolder
                        onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(mContext)
                .inflate(R.layout.recycler_view_horizontal_item,parent,false);

        if(view != null){
            view.setFocusable(true);
        }

        return new HorizontalRecyclerImageAdapter.HorizontalViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HorizontalRecyclerImageAdapter.HorizontalViewHolder holder, int position) {

        RoundedImageView imageView = holder.mImageView;

        Glide.with(mContext)
                .load(new File(mImageList.get(holder.getAdapterPosition())))
                .into(imageView);

        imageView.setOnClickListener
                (view -> mHorizontalImageListener.onImageClicked(holder.getAdapterPosition()));

        Log.i(TAG,"my selected index is " + mSelectedIndex);
        if(getItemViewType(holder.getAdapterPosition()) == NORMAL_ITEM){
            imageView.setBorderColor(Color.WHITE);
            imageView.setAlpha(CONSTANTS.IMAGE_VIEW_ALPHA);
        } else {
            imageView.setBorderColor(Color.RED);
            imageView.setAlpha(1f);
        }
    }

    @Override
    public int getItemCount() {
        if(mImageList == null) {
            return 0;
        } else {
            return mImageList.size();
        }
    }

    @Override
    public int getItemViewType(int position) {
        if(position == mSelectedIndex){
            return SELECTED_ITEM;
        } else {
            return NORMAL_ITEM;
        }
    }

    class HorizontalViewHolder extends RecyclerView.ViewHolder {

        private RoundedImageView mImageView;

        HorizontalViewHolder(View itemView) {
            super(itemView);
            mImageView = itemView.findViewById(R.id.horizontal_recycler_image);
        }
    }
}
