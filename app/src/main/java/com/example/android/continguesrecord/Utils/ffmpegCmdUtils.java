package com.example.android.continguesrecord.Utils;

import android.util.Log;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;

import static android.support.constraint.Constraints.TAG;

public class ffmpegCmdUtils {

    /*
    Fast, but to use only on the same videos encoding.
     */
    public static String[] buildMergeVideosCmd2(String[] videosToMerge, String target){

        String list = generateList(videosToMerge);
        Log.i(TAG,"create the list in the location: "+list);
        return new String[] {
                "-noautorotate",
                "-f",
                "concat",
                "-safe",
                "0",
                "-i",
                list,
                "-c",
                "copy",
                target
        };
    }

    public static String[] cutBeginingCmd(String path,String target){

        return new String[]{
                "-i",
                path,
                "-ss",
                "00:00:00.050",
                "-c",
                "copy",
                target
        };
    }

    /*
    good for mergin land&port but slow
     */
    public static String[] buildMergeWIthKeepAspect(String path1, String path2, String dest){
        return new String[]{
                "-y",
                "-i",
                path1,
                "-i",
                path2,
                "-strict",
                "experimental",
                "-filter_complex",
                "[0:v]scale=iw*min(1920/iw\\,1080/ih):ih*min(1920/iw\\,1080/ih), pad=1920:1080:(1920-iw*min(1920/iw\\,1080/ih))/2:(1080-ih*min(1920/iw\\,1080/ih))/2,setsar=1:1[v0];[1:v] scale=iw*min(1920/iw\\,1080/ih):ih*min(1920/iw\\,1080/ih), pad=1920:1080:(1920-iw*min(1920/iw\\,1080/ih))/2:(1080-ih*min(1920/iw\\,1080/ih))/2,setsar=1:1[v1];[v0][0:a][v1][1:a] concat=n=2:v=1:a=1",
                "-ab",
                "48000",
                "-ac",
                "2",
                "-ar",
                "22050",
                "-s",
                "1920x1080",
                "-vcodec",
                "libx264",
                "-crf",
                "27",
                "-q",
                "4",
                "-preset",
                "ultrafast",
                dest};
    }

    public static String[] buildSlowMotionVideo(String path, String target){
        return new String[]  {
                "-i",
                path,
                "-filter_complex",
                "[0:v]setpts=0.5*PTS[v];[0:a]atempo=2.0[a]",
                "-map",
                "[v]",
                "-map",
                "[a]",
                target
        };
    }

    public static String[] fastForwardVideo(String path,String target){
        return  new String[] {
                "-y",
                "-i",
                path,
                "-filter_complex",
                "[0:v]setpts=0.5*PTS[v];[0:a]atempo=2.0[a]",
                "-map",
                "[v]",
                "-map",
                "[a]",
                "-b:v",
                "2097k",
                "-r",
                "60",
                "-vcodec",
                "mpeg4",
                target
        };
    }

    private static String generateList(String[] inputs) {
        File list;
        Writer writer = null;
        try {
            list = File.createTempFile("ffmpeg-list", ".txt");
            writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(list)));
            for (String input: inputs) {
                writer.write("file '" + input + "'\n");
                Log.d(TAG, "Writing to list file: file '" + input + "'");
            }
        } catch (IOException e) {
            e.printStackTrace();
            return "/";
        } finally {
            try {
                if (writer != null)
                    writer.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }

        Log.d(TAG, "Wrote list file to " + list.getAbsolutePath());
        return list.getAbsolutePath();
    }
}
