package com.example.android.continguesrecord.DB;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.example.android.continguesrecord.MyVideo;

import java.util.List;

public class VideosViewModel extends AndroidViewModel {

    private VideosRepository mRepository;

    private LiveData<List<MyVideo>> mAllVideos;

    public VideosViewModel(@NonNull Application application) {
        super(application);
        mRepository = new VideosRepository(application);
        mAllVideos = mRepository.getAllVideos();
    }

    public LiveData<List<MyVideo>> getAllVideos(){
        return mAllVideos;
    }

    public LiveData<MyVideo> getVideo(long id){
        return mRepository.getVideo(id);
    }

    public void insert(MyVideo myVideo){
        mRepository.insert(myVideo);
    }

    public void delete(MyVideo myVideo){
        mRepository.delete(myVideo);
    }

    public void update(MyVideo myVideo) {
        mRepository.update(myVideo);
    }

}
