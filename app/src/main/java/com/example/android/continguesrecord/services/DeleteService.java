package com.example.android.continguesrecord.services;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.JobIntentService;
import android.util.Log;

import com.example.android.continguesrecord.Utils.CONSTANTS;

import java.io.File;
import java.util.ArrayList;

public class DeleteService extends JobIntentService {

    private final static String TAG = DeleteService.class.getSimpleName();

    public DeleteService() {
        super();
    }

    @Override
    protected void onHandleWork(@NonNull Intent intent) {
        Log.i(TAG,"In the delete service");

        ArrayList<String> videoToDelete = intent.getStringArrayListExtra(CONSTANTS.INTENT_EXTRA_VIDEO_PATH);
        ArrayList<String> imgToDelete = intent.getStringArrayListExtra(CONSTANTS.INTENT_EXTRA_IMG_PATH);

        Log.i(TAG,"video to delete: "+videoToDelete);
        Log.i(TAG,"img to delete: "+imgToDelete);

        File deleteFile = null;

        if(videoToDelete != null) {
            for (String s : videoToDelete) {
                deleteFile = new File(s);
                if (deleteFile.delete()) {
                    Log.i(TAG, "Video file deleted successfully");
                }
            }
        }

        if(imgToDelete != null) {
            for (String s : imgToDelete) {
                deleteFile = new File(s);
                if (deleteFile.delete()) {
                    Log.i(TAG, "Img file deleted successfully");
                }
            }
        }
    }
}
