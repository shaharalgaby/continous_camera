package com.example.android.continguesrecord.DB;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import com.example.android.continguesrecord.MyVideo;

import java.util.List;

public class VideosRepository {

    private VideoDao mVideoDao;
    private LiveData<List<MyVideo>> mAllVideos;

    VideosRepository(Application application){
        VideosRoomDatabase db = VideosRoomDatabase.getDatabase(application);
        mVideoDao = db.videoDao();
        mAllVideos = mVideoDao.getAllVideos();
    }

    LiveData<List<MyVideo>> getAllVideos(){
        return mAllVideos;
    }

    LiveData<MyVideo> getVideo(long id){
        return mVideoDao.getVideo(id);
    }

    void insert (MyVideo myVideo){
        new insertAsyncTask(mVideoDao).execute(myVideo);
    }

    public void delete (MyVideo myVideo){
        new deleteAsyncTask(mVideoDao).execute(myVideo);
    }

    void update(MyVideo myVideo) {
        new updateAsyncTask(mVideoDao).execute(myVideo);
    }

    //AsyncTask for inserting the new object
    private static class insertAsyncTask extends AsyncTask<MyVideo, Void, Void> {

        private VideoDao mAsyncTaskDao;

        insertAsyncTask(VideoDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final MyVideo... params) {
            mAsyncTaskDao.insert(params[0]);
            return null;
        }
    }

    //AsyncTask for inserting delete video object
    private static class deleteAsyncTask extends AsyncTask<MyVideo, Void, Void> {

        private VideoDao mAsyncTaskDao;

        deleteAsyncTask(VideoDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final MyVideo... params) {
            mAsyncTaskDao.delete(params[0]);
            return null;
        }
    }

    //AsyncTask for updating a object
    private static class updateAsyncTask extends AsyncTask<MyVideo, Void, Void> {

        private VideoDao mAsyncTaskDao;

        updateAsyncTask(VideoDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final MyVideo... params) {
            mAsyncTaskDao.update(params[0]);
            return null;
        }
    }

}
