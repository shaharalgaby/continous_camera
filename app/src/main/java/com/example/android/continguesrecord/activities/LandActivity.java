package com.example.android.continguesrecord.activities;

import android.Manifest;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.res.Resources;
import android.graphics.Rect;
import android.support.design.widget.AppBarLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AutoCompleteTextView;
import com.example.android.continguesrecord.DB.VideosViewModel;
import com.example.android.continguesrecord.Utils.ServicesUtils;
import com.example.android.continguesrecord.adapters.LandActivityAdapter;
import com.example.android.continguesrecord.MyVideo;
import com.example.android.continguesrecord.R;
import com.example.android.continguesrecord.Utils.CONSTANTS;
import com.example.android.continguesrecord.listeners.VideoImageClickListener;
import com.example.android.continguesrecord.Utils.MyAnimationUtils;
import com.github.hiteshsondhi88.libffmpeg.FFmpeg;
import com.github.hiteshsondhi88.libffmpeg.LoadBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegNotSupportedException;

import java.util.ArrayList;

public class LandActivity extends AppCompatActivity implements
        View.OnClickListener,
        VideoImageClickListener{

    private static final String TAG = LandActivity.class.getSimpleName();
    private static final int REQUEST_PERMISSIONS = 200;

    private RecyclerView mRecyclerView;
    private LandActivityAdapter mAdapter;
    private Menu mMenu;
    private VideosViewModel mViewModel;
    Handler handler = new Handler();
    private boolean permissionToRecord = true;
    private String[] permissions = {
            Manifest.permission.RECORD_AUDIO,
            Manifest.permission.CAMERA,
            Manifest.permission.WRITE_EXTERNAL_STORAGE};
    private ArrayList<String> permissionsNotApproved = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_land);

        Toolbar mToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        mRecyclerView = findViewById(R.id.recycler_view);
        FloatingActionButton mFloatingActionButton = findViewById(R.id.fab_start_video);

        initViewModel();

        ArrayList<MyVideo> mVideosList = new ArrayList<>();
        mAdapter = new LandActivityAdapter(this, mVideosList,mViewModel,this);

        initRecyclerView();
        initCollapsingToolbar();

        MyAnimationUtils.setOnClickReduceAndRegainSize(this, mFloatingActionButton);

        handler.post(this::loadBinaries);

        checkPermissions();
    }

    public void checkPermissions(){
        for (String permission : permissions) {
            if (ContextCompat.checkSelfPermission
                    (this, permission) != PackageManager.PERMISSION_GRANTED) {
                permissionsNotApproved.add(permission);
            }
        }
        if(permissionsNotApproved.size() > 0){
            String[] permissionsToAsk = new String[permissionsNotApproved.size()];
            for(int i=0;i<permissionsNotApproved.size();i++){
                permissionsToAsk[i] = permissionsNotApproved.get(i);
            }
            ActivityCompat.requestPermissions
                    (this,permissionsToAsk,REQUEST_PERMISSIONS);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode){
            case REQUEST_PERMISSIONS: {
                for(int s : grantResults){
                    if(s == PackageManager.PERMISSION_DENIED){
                        permissionToRecord = false;
                    }
                }
                if(!permissionToRecord){
                    finish();
                    //maybe should show an alert dialog.
                }

                break;
            }
        }
    }

    public void initRecyclerView(){
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(
                this, 2);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.addItemDecoration(new GridSpacingItemDecoration
                (2, dpToPx(10), true));

        RecyclerView.ItemAnimator itemAnimator = new DefaultItemAnimator();
        itemAnimator.setMoveDuration(CONSTANTS.RECYCLER_MOVE_DURATION);

        mRecyclerView.setItemAnimator(itemAnimator);
        mRecyclerView.setAdapter(mAdapter);
    }

    public void loadBinaries(){
        FFmpeg ffmpeg = FFmpeg.getInstance(this);
        try {
            ffmpeg.loadBinary(new LoadBinaryResponseHandler() {

                @Override
                public void onStart() {}

                @Override
                public void onFailure() {}

                @Override
                public void onSuccess() {}

                @Override
                public void onFinish() {}
            });
        } catch (FFmpegNotSupportedException e) {
            // Handle if FFmpeg is not supported by device
        }
    }

    public void initViewModel(){
        mViewModel = ViewModelProviders.of(this).get(VideosViewModel.class);
        mViewModel.getAllVideos().observe(this, myVideos -> mAdapter.setVideos(myVideos));
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();

        switch(id){
            case R.id.fab_start_video:{
                createNewVideo();
                break;
            }
        }
    }

    public void createNewVideo(){

        final String curMillis = System.currentTimeMillis()+"";

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.create_new_video);
        View viewInflated = LayoutInflater.from(this).inflate(R.layout.dialog_new_video,null );
        final AutoCompleteTextView input = viewInflated.findViewById(R.id.input);
        builder.setView(viewInflated);

        builder.setPositiveButton(android.R.string.ok, (dialog, which) -> {
            dialog.dismiss();
            String title = input.getText().toString();
            MyVideo myVideo = new MyVideo(title,
                    curMillis,
                    true,
                    false,
                    -1,
                    -1,
                    0);
            mViewModel.insert(myVideo);
            startCamActivity(myVideo);
        });
        builder.setNegativeButton(android.R.string.cancel, (dialog, which) -> dialog.cancel());

        builder.show();
    }

    public void startCamActivity(MyVideo myVideo){
        Intent intent = new Intent(LandActivity.this, CamActivity.class);

        intent.putExtra(CONSTANTS.INTENT_EXTRA_VIDEO, myVideo);

        startActivity(intent);
    }


    @Override
    public void onImageClicked(MyVideo myVideo) {
        Log.i(TAG,"In onImageClicked method");

        Intent intent = new Intent(this, ExoPlayerActivity.class);

        intent.putExtra(CONSTANTS.INTENT_VIDEO_ID,myVideo.getDateInMills());

        startActivity(intent);
    }

    @Override
    public void onEditClicked(MyVideo myVideo) {

        Intent intent = new Intent(this, CamActivity.class);
        intent.putExtra(CONSTANTS.INTENT_EXTRA_VIDEO, myVideo);

        startActivity(intent);
    }

    @Override
    public void onDownloadClicked(MyVideo myVideo) {
        Log.i(TAG,"Download pressed");
        ServicesUtils.mergeAndSaveToPublicDirectory(this,myVideo);
    }

    /**
     * Initializing collapsing toolbar
     * Will show and hide the toolbar title on scroll
     */
    private void initCollapsingToolbar() {
        AppBarLayout appBarLayout = findViewById(R.id.appbar);
        appBarLayout.setExpanded(true);

        // hiding & showing the title when toolbar expanded & collapsed
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = false;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    showOption(R.id.action_shoot);
                    isShow = true;
                } else if (isShow) {
                    hideOption(R.id.action_shoot);
                    isShow = false;
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.mMenu = menu;
        getMenuInflater().inflate(R.menu.land_menu,menu);
        hideOption(R.id.action_shoot);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        } else if (id == R.id.action_shoot) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void hideOption(int id) {
        MenuItem item = mMenu.findItem(id);
        item.setVisible(false);
    }

    private void showOption(int id) {
        MenuItem item = mMenu.findItem(id);
        item.setVisible(true);
    }

    /**
     * RecyclerView item decoration - give equal margin around grid item
     */
    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        private GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    /**
     * Converting dp to pixel
     */
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }
}
