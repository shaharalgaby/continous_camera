package com.example.android.continguesrecord;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import com.example.android.continguesrecord.Utils.CONSTANTS;

import java.util.ArrayList;

@Entity(tableName = CONSTANTS.TABLE_NAME)
public class MyVideo implements Parcelable {

    //The primary key is the date as its unique for each video.
    @PrimaryKey
    @NonNull
    private String dateInMills;

    private String title;
    private String size;
    private int duration;
    private boolean isNewVideo;
    private boolean isProcessing;
    private int cameraDirection;
    private int cameraOrientation;
    private ArrayList<String> videos = new ArrayList<>();
    private ArrayList<String> images = new ArrayList<>();

    public MyVideo(){
        dateInMills = System.currentTimeMillis()+"";
    }

    public MyVideo(String title, @NonNull String dateInMills, boolean isNew, boolean isProcessing,
                   int cameraDirection,int cameraOrientation, int duration){
        this.title = title;
        this.dateInMills = dateInMills;
        this.isNewVideo = isNew;
        this.isProcessing = isProcessing;
        this.cameraDirection = cameraDirection;
        this.duration = duration;
        this.cameraOrientation = cameraOrientation;
        this.size = "0";
    }


    protected MyVideo(Parcel in) {
        dateInMills = in.readString();
        title = in.readString();
        size = in.readString();
        isNewVideo = in.readByte() != 0;
        isProcessing = in.readByte() != 0;
        cameraDirection = in.readInt();
        cameraOrientation = in.readInt();
        videos = in.createStringArrayList();
        images = in.createStringArrayList();
        duration = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(dateInMills);
        dest.writeString(title);
        dest.writeString(size);
        dest.writeByte((byte) (isNewVideo ? 1 : 0));
        dest.writeByte((byte) (isProcessing ? 1 : 0));
        dest.writeInt(cameraDirection);
        dest.writeInt(cameraOrientation);
        dest.writeStringList(videos);
        dest.writeStringList(images);
        dest.writeInt(duration);
    }

    public static final Creator<MyVideo> CREATOR = new Creator<MyVideo>() {
        @Override
        public MyVideo createFromParcel(Parcel in) {
            return new MyVideo(in);
        }

        @Override
        public MyVideo[] newArray(int size) {
            return new MyVideo[size];
        }
    };

    public int getDuration(){
        return duration;
    }

    public void setDuration(int duration){
        this.duration = duration;
    }

    public int getCameraDirection(){
        return cameraDirection;
    }

    public void setCameraDirection(int value){
        this.cameraDirection = value;
    }

    public int getCameraOrientation() {
        return cameraOrientation;
    }

    public void setCameraOrientation(int cameraOrientation) {
        this.cameraOrientation = cameraOrientation;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public boolean isProcessing(){
        return isProcessing;
    }

    public void setProcessing(boolean val){
        isProcessing = val;
    }

    public boolean isNewVideo() {
        return isNewVideo;
    }

    public void setNewVideo(boolean newVideo) {
        isNewVideo = newVideo;
    }

    @NonNull
    public String getDateInMills() {
        return dateInMills;
    }

    public void setDateInMills(@NonNull String dateInMills) {
        this.dateInMills = dateInMills;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ArrayList<String> getVideos() {
        return videos;
    }

    public ArrayList<String> getImages() {
        return images;
    }

    public void setVideos(ArrayList<String> videos) {
        this.videos = videos;
    }

    public void setImages(ArrayList<String> images) {
        this.images = images;
    }

    public void addPath(String videoPath, String imgPath){
        this.videos.add(videos.size(),videoPath);
        this.images.add(images.size(),imgPath);
    }

    public void removePath(int index){
        videos.remove(index);
        images.remove(index);
    }

    @Override
    public int describeContents() {
        return 0;
    }

}
